﻿using System;
using System.Collections;
using System.Collections.Generic;
using Optima_3_2.ControlStations;
using PropertyGridSettings.Model;

namespace Optima_3_2.ClassesForStation.List
{
    public class ControlStationsList : IListForControlStation, ICollection<IControlStation>
    {
        public ControlStationsList()
        {
            list = new List<IControlStation>();
        }


        #region Properties ICollection

        public int Count => list.Count;

        public bool IsReadOnly => false;

        #endregion

        #region Properties IListForControlStation

        public List<IControlStation> list { get; set; }

        public IControlStation this[string name]
        {
            get => GetStationByName(name);
            set => list.Add(value);
        }

        #endregion

        #region Methods ICollection

        public void Add(IControlStation item)
        {
            list.Add(item);
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(IControlStation item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(IControlStation[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(IControlStation item)
        {
            foreach (var station in list)
                if (station.StationModel.Name.Equals(item.StationModel.Name))
                    return list.Remove(station);
            return false;
        }

        public IEnumerator<IControlStation> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private IControlStation GetStationByName(string name)
        {
            foreach (var station in list)
                if (station.StationModel.Name.Equals(name))
                    return station;
            return null;
        }

        #endregion

        #region Methods IListForControlStation

        public void SetCoordinateViewForAllModels(ViewCoord viewCoord)
        {
            foreach (var station in list)
                station.SetCoordView(viewCoord);
        }

        public bool IsMainControlStationExist()
        {
            foreach (var station in list) 
                if (station.StationModel.Name.Equals("Main"))
                    return true;
            return false;
        }
        #endregion
    }
}