﻿using System.Collections.Generic;
using Optima_3_2.ControlStations;
using PropertyGridSettings.Model;

namespace Optima_3_2.ClassesForStation.List
{
    public interface IListForControlStation
    {
        #region Properties

        List<IControlStation> list { get; set; }

        IControlStation this[string name] { get; set; }

        #endregion

        #region Methods

        void SetCoordinateViewForAllModels(ViewCoord viewCoord);

        bool IsMainControlStationExist();

        #endregion
    }
}