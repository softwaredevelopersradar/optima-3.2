﻿using System;
using System.Collections.Generic;
using System.Windows;
using GrozaSModelsDBLib;
using Mapsui.Projection;
using Optima_3_2.ControlStations;
using PropertyGridControlStation.Model;
using WpfMapControl;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        #region PropertyGrid Events

        private void ControlStation_OnIpAddressChanged(object sender, string e)
        {
            gSMConnection.LocalIP = e;
            radioConnection.LocalIP = e;
        }

        private void ControlStationPropertyGrid_OnDeleteButtonClick(object sender, ControlStationModel e)
        {
            propertyGridControlStation.Visibility = Visibility.Collapsed;

            DeleteButtonClicked(e);

            SaveControlStations();

            RasterMap.mapControl.IsEnabled = true;
        }

        private void ControlStationPropertyGrid_OnApplyButtonClick(object sender, ControlStationModel e)
        {
            propertyGridControlStation.Visibility = Visibility.Collapsed;

            var controlStation = controlStationsList[e.Name];
            if (controlStation != null)
            {
                controlStation.StationModel = e.Clone();
            }
            else
            {
                controlStation = new ControlStationObject(e, propertiesPropGrid.Local.Common.CoordinateView);
                controlStationsList.Add(controlStation);
            }

            ApplyButtonClicked(e);

            SaveControlStations();

            RasterMap.mapControl.IsEnabled = true;
        }


        private void DeleteButtonClicked(BaseControlStationModel e)
        {
            RasterMap.mapControl.RemoveObject(controlStationsList[e.Name]?.StaitionImage);
            RasterMap.ChangeIconOfMenuItemStation(e.Name,
                Environment.CurrentDirectory + "/Resources/plus.png");

            controlStationsList.Remove(controlStationsList[e.Name]);


            if (e.IsStationNowInUse)
            {
                if (controlStationsList.IsMainControlStationExist())
                    ChangeIsNowInUseParametr("Main", true);
                else if (controlStationsList.Count > 0)
                    ChangeIsNowInUseParametr(controlStationsList.list[0].StationModel.Name, true);
            }
        }

        private void ApplyButtonClicked(BaseControlStationModel e)
        {
            if (e.IsStationNowInUse)
                GivePermissionForMainStation(e.Name);
            else DrawControlStation(controlStationsList[e.Name], false);

            AddControlPointToPripherial(e);

            RasterMap.ChangeIconOfMenuItemStation(e.Name, Environment.CurrentDirectory + "/Resources/update.png");
        }

        private void AddControlPointToPripherial(BaseControlStationModel e)
        {
            if (e.IsStationNowInUse)
            {
                var coord = new Coord();
                coord.Latitude = e.Latitude;
                coord.Longitude = e.Longitude;
                UpdateJammerMap(coord);


                RasterMap.GNSSPosition = new Location(e.Longitude, e.Latitude);
            }
        }

        private void GivePermissionForMainStation(string nameOfNewMainStation)
        {
            if (RasterMap.mapControl.IsMapLoaded)
                foreach (var station in controlStationsList)
                    ChangeIsNowInUseParametr(station.StationModel.Name,
                        station.StationModel.Name == nameOfNewMainStation);
        }


        private IControlStation FindMainStation()
        {
            foreach (var station in controlStationsList)
                if (station.StationModel.IsStationNowInUse)
                    return station;

            return null;
        }

        #endregion


        #region Map Events

        private void RasterMap_MenuItemMainStationClick(object sender, string e)
        {
            propertyGridStation.Visibility = Visibility.Collapsed;
            propertyGridControlStation.Visibility = Visibility.Visible;

            if (UpdateOldControlStation(e))
                return;

            CreateNewControlStation(e);


            RasterMap.mapControl.IsEnabled = false;
        }

        private bool UpdateOldControlStation(string name)
        {
            var controlStation = controlStationsList[name];
            if (controlStation != null)
                if (controlStation.GetType() == typeof(ControlStationObject))
                {
                    propertyGridControlStation.Local = (ControlStationModel)controlStation.StationModel;
                    return true;
                }

            return false;
        }


        private void CreateNewControlStation(string name)
        {
            var controlStationModel = new ControlStationModel();
            controlStationModel.Longitude = RasterMap.ClickCoord.Longitude;
            controlStationModel.Latitude = RasterMap.ClickCoord.Latitude;
            controlStationModel.Name = name;
            controlStationModel.ViewCoordField = propertiesPropGrid.Local.Common.CoordinateView;

            propertyGridControlStation.Local.Update(controlStationModel);
        }

        #endregion


        #region Save control stations

        private void SaveControlStations()
        {
            try
            {
                var stationModels = new List<ControlStationModel>();
                foreach (var station in controlStationsList)
                    stationModels.Add((ControlStationModel)station.StationModel);

                Yaml.YamlSave(stationModels, "ControlStations.yaml");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void LoadControlStations()
        {
            try
            {
                var stationModels = Yaml.YamlLoad<List<ControlStationModel>>("ControlStations.yaml");
                if (stationModels != null)
                    foreach (var model in stationModels)
                    {
                        controlStationsList.Add(new ControlStationObject(model,
                            propertiesPropGrid.Local.Common.CoordinateView));

                        AddControlPointToPripherial(model);

                        RasterMap.ChangeIconOfMenuItemStation(model.Name,
                            Environment.CurrentDirectory + "/Resources/update.png");
                    }
            }
            catch
            {
                Console.WriteLine("ControlStations.yaml does not exist");
            }

            DrawAllControlStation();
        }

        #endregion


        #region Draw control stations

        private void DrawControlStation(IControlStation controlStation, bool IsStationInUse)
        {
            RasterMap.mapControl.RemoveObject(controlStation.StaitionImage);

            var mainPoint = Mercator.FromLonLat(controlStation.StationModel.Longitude,
                controlStation.StationModel.Latitude);

            if (controlStation.StationModel.Note.Equals(" ") || controlStation.StationModel.Note.Equals(""))
                controlStation.StaitionImage = RasterMap.mapControl.AddMapObject(
                    IsStationInUse ? _subControlStationStyle : _controlStationStyle, controlStation.StationModel.Name,
                    mainPoint);
            else
                controlStation.StaitionImage = RasterMap.mapControl.AddMapObject(
                    IsStationInUse ? _subControlStationStyleWithNote : _controlStationStyleWithNote,
                    controlStation.StationModel.Name + "\n  " + controlStation.StationModel.Note, mainPoint);
        }


        private void DrawAllControlStation()
        {
            if (RasterMap.mapControl.IsMapLoaded)
                foreach (var station in controlStationsList)
                    DrawControlStation(station, station.StationModel.IsStationNowInUse);
        }


        private void ChangeIsNowInUseParametr(string name, bool isNowInUse)
        {
            var station = controlStationsList[name];
            if (station != null)
            {
                station.StationModel.IsStationNowInUse = isNowInUse;
                DrawControlStation(station, isNowInUse);
            }
        }

        #endregion
    }
}