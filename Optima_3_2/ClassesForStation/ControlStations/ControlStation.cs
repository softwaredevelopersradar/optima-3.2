﻿using Optima_3_2.ControlStations;
using PropertyGridControlStation.Model;
using PropertyGridSettings.Model;
using WpfMapControl;
using YamlDotNet.Serialization;

namespace Optima_3_2
{
    public class ControlStationObject : IControlStation
    {
        private ControlStationModel stationModel;

        #region Methods IControlStation

        public void SetCoordView(ViewCoord viewCoord)
        {
            StationModel.ViewCoordField = viewCoord;
        }

        #endregion

        #region Constructors

        public ControlStationObject(ControlStationModel controlStationModel, ViewCoord viewCoord)
        {
            StationModel = controlStationModel;
            StationModel.ViewCoordField = viewCoord;
        }

        public ControlStationObject(Location location, ViewCoord viewCoord, string name)
        {
            StationModel = new ControlStationModel();
            StationModel.Latitude = location.Latitude;
            StationModel.Longitude = location.Longitude;
            StationModel.Name = name;
            StationModel.ViewCoordField = viewCoord;
            if (name.Equals("Main"))
                StationModel.IsStationNowInUse = true;
        }

        #endregion

        #region Properties IControlStation

        [YamlIgnore] public IMapObject StaitionImage { get; set; }

        public BaseControlStationModel StationModel
        {
            get => stationModel;
            set => stationModel = ((ControlStationModel)value).Clone();
        }

        #endregion
    }
}