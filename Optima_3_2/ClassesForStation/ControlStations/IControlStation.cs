﻿using PropertyGridControlStation.Model;
using PropertyGridSettings.Model;
using WpfMapControl;

namespace Optima_3_2.ControlStations
{
    public interface IControlStation
    {
        #region Properties
        IMapObject StaitionImage { get; set; }

        BaseControlStationModel StationModel { get; set; }
        #endregion

        #region Methods
        void SetCoordView(ViewCoord viewCoord);
        #endregion
    }
}