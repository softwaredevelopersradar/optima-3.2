﻿using WpfMapControl;

namespace Optima_3_2.ControlStations
{
    public interface IDrawControlStation
    {
        #region

        IMapObject StaitionImage { get; set; }

        #endregion
    }
}