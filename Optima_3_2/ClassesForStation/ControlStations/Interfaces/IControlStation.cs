﻿using PropertyGridControlStation.Model;
using PropertyGridSettings.Model;

namespace Optima_3_2.ControlStations
{
    public interface IControlStation : IDrawControlStation
    {
        #region Properties

        BaseControlStationModel StationModel { get; set; }

        #endregion

        #region Methods

        void SetCoordView(ViewCoord viewCoord);

        #endregion
    }
}