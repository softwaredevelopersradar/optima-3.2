﻿using System;
using System.Collections;
using System.Collections.Generic;
using Optima_3_2.ClassesForStation.Stations.List;

namespace Optima_3_2.ClassesForStation.Stations.Connection
{
    public class ListGSMConnection : IListGSMConnection
    {
        public ListGSMConnection()
        {
            gSMConnections = new List<IGSMIsueForStation>();
        }

        public ListGSMConnection(StationsList stations)
        {
            gSMConnections = new List<IGSMIsueForStation>();
            CopyFromStationsList(stations, true);
        }

        public ListGSMConnection(StationsList stations, bool isSelectAll)
        {
            gSMConnections = new List<IGSMIsueForStation>();
            CopyFromStationsList(stations, isSelectAll);
        }

        #region Properties IListGSMConnection

        public List<IGSMIsueForStation> gSMConnections { get; set; }

        #endregion

        #region Properties IList

        public IGSMIsueForStation this[int index]
        {
            get => gSMConnections[index];
            set => gSMConnections[index] = value;
        }

        public int Count => gSMConnections.Count;

        public bool IsReadOnly => false;

        #endregion

        #region Methods IList

        public void Add(IGSMIsueForStation item)
        {
            gSMConnections.Add(item);
        }

        public void Clear()
        {
            gSMConnections.Clear();
        }

        public bool Contains(IGSMIsueForStation item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(IGSMIsueForStation[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(IGSMIsueForStation item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, IGSMIsueForStation item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(IGSMIsueForStation item)
        {
            return gSMConnections.Remove(item);
        }

        public void RemoveAt(int index)
        {
            gSMConnections.RemoveAt(index);
        }

        public IEnumerator<IGSMIsueForStation> GetEnumerator()
        {
            return gSMConnections.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Method IGSMConnection

        public void CopyFromStationsList(StationsList stations, bool isSelectAll)
        {
            var cloneStation = new List<IStation>(stations.list);

            if (isSelectAll)
                CopyAllFromSationsList(cloneStation);
            else CopyFromSationsList(cloneStation);
        }

        public void CopyAllFromSationsList(List<IStation> cloneStation)
        {
            foreach (var station in cloneStation) gSMConnections.Add(station.GSMIsueForStation.Clone());
        }

        public void CopyFromSationsList(List<IStation> cloneStation)
        {
            foreach (var station in cloneStation)
                if (station.StationModel.IsActive)
                    gSMConnections.Add(station.GSMIsueForStation.Clone());
        }


        public void ClearAllFlagsForAllStations()
        {
            foreach (var item in gSMConnections)
                item.ClearAllFlagsForGSMConnection();
        }


        public void ClearAllCountersForAllStations()
        {
            foreach (var item in gSMConnections)
                item.ClearAllCountersGSMConnection();
        }

        public bool IsSelectedStationsEndInitializationPart()
        {
            for (var i = 0; i < gSMConnections.Count; i++)
                if (!gSMConnections[i].IsInitialized && !gSMConnections[i].IsSomeErrorOccurred)
                    return false;
            return true;
        }

        public bool IsAllSocketGetMessageOrError()
        {
            foreach (var item in gSMConnections)
                if (item.IsSomeMessageNotSent && !item.IsSomeMessagesReaded && item.IsInitialized)
                    return false;
            return true;
        }


        public IGSMIsueForStation GetBySocketNumber(int socketNumber)
        {
            foreach (var item in gSMConnections)
                if (item.SocketNumber == socketNumber)
                    return item;
            return null;
        }

        public void RemoveAtSocketNumber(int socketNumber)
        {
            for (var i = 0; i < gSMConnections.Count; i++)
                if (gSMConnections[i].SocketNumber == socketNumber)
                    RemoveAt(i);
        }

        #endregion
    }
}