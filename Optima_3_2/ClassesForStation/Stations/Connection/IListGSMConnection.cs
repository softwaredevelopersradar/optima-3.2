﻿using System.Collections.Generic;
using Optima_3_2.ClassesForStation.Stations.List;

namespace Optima_3_2.ClassesForStation.Stations.Connection
{
    public interface IListGSMConnection : IList<IGSMIsueForStation>
    {
        #region Properties

        List<IGSMIsueForStation> gSMConnections { get; set; }

        #endregion

        #region Methods

        void CopyFromStationsList(StationsList stations, bool isSelectAll);

        void ClearAllFlagsForAllStations();

        void ClearAllCountersForAllStations();

        bool IsSelectedStationsEndInitializationPart();

        IGSMIsueForStation GetBySocketNumber(int socketNumber);

        bool IsAllSocketGetMessageOrError();

        #endregion
    }
}