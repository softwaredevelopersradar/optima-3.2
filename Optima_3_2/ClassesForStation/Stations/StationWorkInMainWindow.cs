﻿using System;
using System.Collections.Generic;
using System.Windows;
using Mapsui.Projection;
using ModemControl;
using PropertyGridStation.Model;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        #region Table event

        private void Table_OnSelectionChanged(object sender, ModemModel e)
        {
            if(e.Latitude == -500 || e.Longitude == -500)
                return;
            
            var p = Mercator.FromLonLat(e.Longitude, e.Latitude);
            RasterMap.mapControl.NavigateTo(p);
            //stationsList[e.Number].StationModel.IsActive = e.IsActive;
        }

        #endregion


        #region Update model

        private void Station_OnUpdateModemModel(ModemModel e)
        {
            UpdateModemModel(e);
        }

        private void UpdateModemModel(ModemModel e)
        {
            for (var i = 0; i < listModem.Count; i++)
                if (listModem[i].Number == e.Number)
                {
                    listModem[i] = e;
                    Table.ListModemModel = listModem;
                    return;
                }

            listModem.Add(e);
            Table.ListModemModel = listModem;
        }

        #endregion


        #region PropertyGrid Events

        private void propertyGridStation_OnApplyButtonClick(object sender, EHS6Model e)
        {
            var station = stationsList[e.Number];
            if (station != null)
            {
                station.StationModel = e.Clone();
            }
            else
            {
                station = new EHS6(e.Clone(), propertiesPropGrid.Local.Common.CoordinateView);
                station.OnUpdateModemModel += Station_OnUpdateModemModel;
                stationsList.Add(station);
            }

            ApplyButtonClicked(station);

            SaveStations();

            propertyGridStation.Visibility = Visibility.Collapsed;
            RasterMap.mapControl.IsEnabled = true;
        }

        private void propertyGridStation_OnDeleteButtonClick(object sender, EHS6Model e)
        {
            DeleteOneStation(e.Clone());

            propertyGridStation.Visibility = Visibility.Collapsed;
            RasterMap.mapControl.IsEnabled = true;
        }

        private void propertyGridStation_OnExitButtonClick(object sender, EHS6Model e)
        {
            propertyGridStation.Visibility = Visibility.Collapsed;
            RasterMap.mapControl.IsEnabled = true;
        }

        private void ApplyButtonClicked(IStation station)
        {
            RasterMap.ChangeIconOfMenuItemStation(station.StationModel.Number - 1,
                Environment.CurrentDirectory + "/Resources/update.png");


            DrawStation(station);
            DrawIndicationOfConnectionState(station, station.StationStatus);
            DrawIndicatorOfSignalLevel(station, station.SignalLevel);

            station.UpdateModemModel();
            Table.ChangeSelectedIndex(station.StationModel.Number);
            listModem.Sort(SortStations_Table);
            Table.ListModemModel = listModem;
        }


        private void DeleteOneStation(ModemModel model)
        {
            try
            {
                DeleteDrawings(stationsList[model.Number]);

                var index = stationsList.IndexOf(model.Number);
                if (index == -1) return;

                listModem.RemoveAt(index);
                stationsList.RemoveAt(model.Number);
                Table.ListModemModel = listModem;
                SaveStations();
            }
            catch
            {
            }


            RasterMap.ChangeIconOfMenuItemStation(model.Number - 1,
                Environment.CurrentDirectory + "/Resources/plus.png");
        }

        private static int SortStations_Table(ModemModel modemModel1, ModemModel modemModel2)
        {
            if (modemModel1.Number == modemModel2.Number)
                return 0;
            var retval = modemModel1.Number.CompareTo(modemModel2.Number);
            return retval;
        }

        #endregion


        #region Map Events

        private void RasterMap_MenuItemStationClick(object sender, int e)
        {
            propertyGridControlStation.Visibility = Visibility.Collapsed;
            propertyGridStation.Visibility = Visibility.Visible;

            if (UpdateOldModemModel(e))
                return;

            CreateNewEHS6Model(e);
        }

        private bool UpdateOldModemModel(int e)
        {
            var station = stationsList[e];
            if (station != null)
                if (station.GetType() == typeof(EHS6))
                {
                    propertyGridStation.Local.Update((EHS6Model)station.StationModel);
                    return true;
                }

            return false;
        }

        private void CreateNewEHS6Model(int e)
        {
            var model = new EHS6Model();
            model.Latitude = RasterMap.ClickCoord.Latitude;
            model.Longitude = RasterMap.ClickCoord.Longitude;
            model.Number = (byte)e;
            model.ViewCoordField = propertiesPropGrid.Local.Common.CoordinateView;
            propertyGridStation.Local.Update(model);
        }


        private void RasterMap_DeleteAllStationClick(object sender, EventArgs e)
        {
            DeleteAllDrawings();

            listModem.Clear();
            stationsList.Clear();
            Table.ListModemModel = listModem;
            SaveStations();
            ChangeAllStationIconOfMapMenuItem();
        }

        #endregion


        #region Save stations

        private void SaveStations()
        {
            Yaml.YamlSave(Table.ListModemModel, "Stations.yaml");
        }

        private void LoadStations()
        {
            try
            {
                var modemModels = Yaml.YamlLoad<List<EHS6Model>>("Stations.yaml");

                foreach (var model in modemModels)
                {
                    var eHS6 = new EHS6(model, propertiesPropGrid.Local.Common.CoordinateView);
                    eHS6.OnUpdateModemModel += Station_OnUpdateModemModel;
                    stationsList.Add(eHS6);
                    var station = stationsList[model.Number];
                    station.UpdateModemModel();
                    ApplyButtonClicked(station);
                }
            }
            catch
            {
                Console.WriteLine("ControlStations.yaml does not exist");
            }
        }

        #endregion


        #region Draw stations

        private void DrawStation(IStation station)
        {
            if (!RasterMap.mapControl.IsMapLoaded) return;

            RasterMap.mapControl.RemoveObject(station?.StationImage);
            DeleteALLSignalLines(station);

            var stationPoint = Mercator.FromLonLat(station.StationModel.Longitude, station.StationModel.Latitude);
            if (station.StationModel.Note.Equals(" ") || station.StationModel.Note.Equals(""))
                station.StationImage =
                    RasterMap.mapControl.AddMapObject(_stationStyle, stationName + " " + station.StationModel.Number,
                        stationPoint);
            else
                station.StationImage = RasterMap.mapControl.AddMapObject(_stationStyleWithNote,
                    stationName + " " + station.StationModel.Number + "\n  " + station.StationModel.Note, stationPoint);
        }

        private void DrawStation(int j)
        {
            if (!RasterMap.mapControl.IsMapLoaded) return;

            RasterMap.mapControl.RemoveObject(stationsList[j]?.StationImage);
            DeleteALLSignalLines(stationsList[j]);

            var stationPoint = Mercator.FromLonLat(stationsList[j].StationModel.Longitude,
                stationsList[j].StationModel.Latitude);
            if (stationsList[j].StationModel.Note.Equals(" ") || stationsList[j].StationModel.Note.Equals(""))
                stationsList[j].StationImage = RasterMap.mapControl.AddMapObject(_stationStyle,
                    stationName + " " + stationsList[j].StationModel.Number, stationPoint);
            else
                stationsList[j].StationImage = RasterMap.mapControl.AddMapObject(_stationStyleWithNote,
                    stationName + " " + stationsList[j].StationModel.Number + "\n  " +
                    stationsList[j].StationModel.Note, stationPoint);
        }


        private void DrawAllStations()
        {
            if (stationsList != null)
                if (stationsList.Count > 0)
                    foreach (var station in stationsList.list)
                        DrawStation(station);
        }

        private void DeleteDrawings(IStation station)
        {
            RasterMap.mapControl.RemoveObject(station?.StationImage);
            RasterMap.mapControl.RemoveObject(station?.StationPolyLine);
            DeleteALLSignalLines(station);
        }

        private void DeleteALLSignalLines(IStation station)
        {
            if(station == null)
                return;
            for(int i = 0; i < station.SignalLines.Length; i++)
                RasterMap.mapControl.RemoveObject(station.SignalLines[i]);
        }

        private void DeleteAllDrawings()
        {
            foreach (var station in stationsList) DeleteDrawings(station);
        }

        #endregion
    }
}