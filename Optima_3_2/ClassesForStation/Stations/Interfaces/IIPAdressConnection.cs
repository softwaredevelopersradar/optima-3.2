﻿namespace Optima_3_2.ClassesForStation.Stations.Connection
{
    public interface IIPAdressConnection
    {
        int Port { get; set; }

        string IPAddress { get; set; }
    }
}