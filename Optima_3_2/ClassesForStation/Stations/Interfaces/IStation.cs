﻿using ModemControl;
using ModemControl.Interfaces;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.Enums;
using PropertyGridSettings.Model;

namespace Optima_3_2
{
    public delegate void UpdateHandler(ModemModel e);

    public interface IStation : IDrawStation, IModelMethods<IStation>
    {
        #region Events

        event UpdateHandler OnUpdateModemModel;

        #endregion


        #region Properties

        ModemModel StationModel { get; set; }

        int SignalLevel { get; set; }

        StationStatus StationStatus { get; set; }

        IGSMIsueForStation GSMIsueForStation { get; set; }

        #endregion


        #region Methods

        void UpdateModemModel();

        void SetCoordView(ViewCoord viewCoord);

        void LedsColor(bool GOLONAS_L1, bool GOLONAS_L2, bool GPS_L1, bool GPS_L2, bool GSM, bool RadioF_F1,
            bool RadioF_F2, bool Galileo_L1, bool Galileo_L2, bool Beidou_L1, bool Beidou_L2, bool Radio, bool RM,
            bool Existance_L1, bool Existance_L2);

        void AllLedColor(Led color);

        #endregion
    }
}