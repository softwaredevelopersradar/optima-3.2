﻿using ModemControl.Interfaces;

namespace Optima_3_2.ClassesForStation.Stations.Connection
{
    public interface IGSMIsueForStation : IIPAdressConnection, IModelMethods<IGSMIsueForStation>
    {
        #region Properties

        byte SocketNumber { get; set; }

        bool IsIgnore { get; set; }

        bool IsInitialized { get; set; }

        bool IsFirstMessageSent { get; set; }

        bool IsCheckedWithBadResult { get; set; }

        bool IsSomeMessagesReaded { get; set; }

        bool IsSomeMessageNotSent { get; set; }

        bool IsSomeMessageReceived { get; set; }

        bool IsSomeErrorOccurred { get; set; }


        int AmountOfReInitialization { get; set; }

        int AmountOfCheckedWithBadResult { get; set; }

        int AmountOfNotReceivedMessages { get; set; }

        #endregion


        #region Methods

        bool IsReadyForInitialization(int permittedAmountOfReinitialization);

        bool IsCanNotInitialization(int permittedAmountOfReinitialization);

        bool IsReadyForSend();

        bool IsReadyForChecked();

        bool IsReadyForRead();

        bool IsReadyForReadAll();

        void ClearAllFlagsForGSMConnection();

        void ClearAllCountersGSMConnection();

        #endregion
    }
}