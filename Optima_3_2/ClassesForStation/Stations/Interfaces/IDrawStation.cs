﻿using Mapsui.Geometries;
using Mapsui.Providers;
using WpfMapControl;

namespace Optima_3_2
{
    public interface IDrawStation
    {
        #region Properties

        IMapObject StationImage { get; set; }

        IFeature StationPolyLine { get; set; }

        IFeature[] SignalLines { get; set; }

        #endregion

        #region Methods

        Point[] DrawRange();

        Point[] DrawLines(Location firstPoint, Location secondPoint);

        #endregion
    }
}