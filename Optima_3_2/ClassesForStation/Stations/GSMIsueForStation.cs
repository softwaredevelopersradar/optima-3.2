﻿using ModemControl.Interfaces;
using Optima_3_2.ClassesForStation.Stations.Connection;
using YamlDotNet.Serialization;

namespace Optima_3_2
{
    public class GSMIsueForStation : IGSMIsueForStation
    {
        #region Properties IGSMConnection

        [YamlIgnore] public byte SocketNumber { get; set; }

        [YamlIgnore] public bool IsIgnore { get; set; } = false;

        [YamlIgnore] public bool IsInitialized { get; set; }
        [YamlIgnore] public bool IsFirstMessageSent { get; set; }
        [YamlIgnore] public bool IsCheckedWithBadResult { get; set; }
        [YamlIgnore] public bool IsSomeMessagesReaded { get; set; }
        [YamlIgnore] public bool IsSomeMessageNotSent { get; set; } = true;
        [YamlIgnore] public bool IsSomeMessageReceived { get; set; }
        [YamlIgnore] public bool IsSomeErrorOccurred { get; set; } = false;

        [YamlIgnore] public int AmountOfReInitialization { get; set; }
        [YamlIgnore] public int AmountOfCheckedWithBadResult { get; set; }
        [YamlIgnore] public int AmountOfNotReceivedMessages { get; set; }

        [YamlIgnore] public int Port { get; set; }
        [YamlIgnore] public string IPAddress { get; set; } = "127.0.0.1";

        #endregion

        #region Constructors

        public GSMIsueForStation(byte socketNumber)
        {
            SocketNumber = socketNumber;
        }

        public GSMIsueForStation(byte socketNumber, string iPAddress, int port)
        {
            IPAddress = iPAddress;
            Port = port;
            SocketNumber = socketNumber;
        }

        #endregion

        #region Methids IGSMConnection

        public bool IsReadyForInitialization(int permittedAmountOfReinitialization)
        {
            if (AmountOfReInitialization < permittedAmountOfReinitialization && !IsInitialized && !IsIgnore)
                return true;
            return false;
        }

        public bool IsReadyForSend()
        {
            if (IsInitialized && !IsIgnore)
                return true;
            return false;
        }

        public bool IsReadyForRead()
        {
            if (IsSomeMessageReceived && IsInitialized && IsFirstMessageSent)
                return true;
            return false;
        }

        public bool IsReadyForReadAll()
        {
            if (IsInitialized)
                return true;
            return false;
        }

        public bool IsCanNotInitialization(int permittedAmountOfReinitialization)
        {
            if (AmountOfReInitialization >= permittedAmountOfReinitialization)
                return true;
            return false;
        }

        public bool IsReadyForChecked()
        {
            if (IsInitialized && IsFirstMessageSent && !IsIgnore)
                return true;
            return false;
        }

        public void ClearAllFlagsForGSMConnection()
        {
            //ClearAllCountersGSMConnection();
            IsCheckedWithBadResult = false;
            IsInitialized = false;
            IsFirstMessageSent = false;
        }

        public void ClearAllCountersGSMConnection()
        {
            AmountOfCheckedWithBadResult = 0;
            AmountOfNotReceivedMessages = 0;
            AmountOfReInitialization = 0;
        }

        #endregion

        #region Methods IModelMethods IGSMConnection

        public bool EqualTo(IGSMIsueForStation model)
        {
            return model.IsSomeMessageNotSent.Equals(IsSomeMessageNotSent)
                   || model.IsSomeMessagesReaded.Equals(IsSomeMessagesReaded)
                   || model.IsSomeMessageReceived.Equals(IsSomeMessageReceived)
                   || model.IsInitialized.Equals(IsInitialized)
                   || model.AmountOfCheckedWithBadResult.Equals(AmountOfCheckedWithBadResult)
                   || model.AmountOfNotReceivedMessages.Equals(AmountOfNotReceivedMessages)
                   || model.AmountOfReInitialization.Equals(AmountOfReInitialization)
                   || model.SocketNumber.Equals(SocketNumber)
                   || model.Port.Equals(Port)
                   || model.IPAddress.Equals(IPAddress);
        }

        IGSMIsueForStation IModelMethods<IGSMIsueForStation>.Clone()
        {
            return new GSMIsueForStation(SocketNumber)
            {
                IsInitialized = IsInitialized,
                AmountOfCheckedWithBadResult = AmountOfCheckedWithBadResult,
                AmountOfNotReceivedMessages = AmountOfNotReceivedMessages,
                AmountOfReInitialization = AmountOfReInitialization,
                IsCheckedWithBadResult = IsCheckedWithBadResult,
                IsFirstMessageSent = IsFirstMessageSent,
                IsSomeMessagesReaded = IsSomeMessagesReaded,
                IsSomeMessageNotSent = IsSomeMessageNotSent,
                IsSomeMessageReceived = IsSomeMessageReceived,
                SocketNumber = SocketNumber,
                Port = Port,
                IPAddress = IPAddress
            };
        }

        public void Update(IGSMIsueForStation model)
        {
            IsInitialized = model.IsInitialized;
            AmountOfCheckedWithBadResult = model.AmountOfCheckedWithBadResult;
            AmountOfNotReceivedMessages = model.AmountOfNotReceivedMessages;
            AmountOfReInitialization = model.AmountOfReInitialization;
            IsCheckedWithBadResult = model.IsCheckedWithBadResult;
            IsFirstMessageSent = model.IsFirstMessageSent;
            IsSomeMessagesReaded = model.IsSomeMessagesReaded;
            IsSomeMessageNotSent = model.IsSomeMessageNotSent;
            IsSomeMessageReceived = model.IsSomeMessageReceived;
            SocketNumber = model.SocketNumber;
            Port = model.Port;
            IPAddress = model.IPAddress;
        }

        #endregion
    }
}