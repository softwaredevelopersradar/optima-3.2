﻿using Mapsui.Geometries;
using Mapsui.Providers;
using ModemControl;
using ModemControl.Interfaces;
using Optima_3_2.Enums;
using PropertyGridSettings.Model;
using System;
using WpfMapControl;

namespace Optima_3_2
{
    public delegate void UpdateHandler(ModemModel e);
    public interface IStation : IModelMethods<IStation>
    {

        #region Events
        event UpdateHandler OnUpdateModemModel;
        #endregion


        #region Properties
        IMapObject StationImage { get; set; }

        IFeature StationPolyLine { get; set; }

        IFeature StationLine { get; set; }

        ModemModel StationModel { get; set; }

        int SignalLevel { get; set; }

        StationStatus StationStatus { get; set; }
        #endregion


        #region Methods
        void LedsColor(bool GOLONAS_L1, bool GOLONAS_L2, bool GPS_L1, bool GPS_L2, bool GSM, bool RadioF_F1,
            bool RadioF_F2, bool Galileo_L1, bool Galileo_L2, bool Beidou_L1, bool Beidou_L2, bool Radio, bool RM,
            bool Existance_L1, bool Existance_L2);

        void AllLedColor(Led color);

        void UpdateModemModel();

        void SetCoordView(ViewCoord viewCoord);

        Point[] DrawRange();

        Point[] DrawLines(Point ControlStationPoint);
        #endregion
    }
}