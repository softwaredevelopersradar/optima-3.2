﻿using System.Collections.Generic;
using PropertyGridSettings.Model;

namespace Optima_3_2.ClassesForStation.Stations.List
{
    public interface IListForStation
    {
        #region Properties

        List<IStation> list { get; set; }

        #endregion

        #region Methods

        void SetCoordinateViewForAllModels(ViewCoord viewCoord);

        #endregion
    }
}