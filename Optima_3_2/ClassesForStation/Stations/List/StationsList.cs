﻿using System;
using System.Collections;
using System.Collections.Generic;
using PropertyGridSettings.Model;

namespace Optima_3_2.ClassesForStation.Stations.List
{
    public class StationsList : IListForStation, IList<IStation>
    {
        public StationsList()
        {
            list = new List<IStation>();
        }

        #region Properties IListForStation

        public List<IStation> list { get; set; }

        #endregion

        #region Methods IListForStation

        public void SetCoordinateViewForAllModels(ViewCoord viewCoord)
        {
            foreach (var station in list)
                station.SetCoordView(viewCoord);
        }

        #endregion

        #region Properties IList

        public int Count => list.Count;

        public bool IsReadOnly => false;

        public IStation this[int number]
        {
            get => GetStationByNumber(number);
            set
            {
                list.Add(value);
                SortByNumber();
            }
        }

        #endregion

        #region Methods IList

        public void Clear()
        {
            list.Clear();
        }

        public void RemoveAt(int number)
        {
            for (var j = 0; j < list.Count; j++)
                if (list[j].StationModel.Number == number)
                    list.RemoveAt(j);
        }

        public int IndexOf(IStation item)
        {
            for (var j = 0; j < list.Count; j++)
                if (list[j].StationModel.Number == item.StationModel.Number)
                    return j;
            return -1;
        }

        public int IndexOf(int number)
        {
            for (var j = 0; j < list.Count; j++)
                if (list[j].StationModel.Number == number)
                    return j;
            return -1;
        }

        public void Insert(int index, IStation item)
        {
            list.Insert(IndexOf(item), item);
        }

        public void Add(IStation item)
        {
            list.Add(item);
            SortByNumber();
        }

        public bool Contains(IStation item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(IStation[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(IStation item)
        {
            return list.Remove(GetStationByNumber(item.StationModel.Number));
        }

        public IEnumerator<IStation> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Methods private

        private IStation GetStationByNumber(int number)
        {
            foreach (var station in list)
                if (station.StationModel.Number == number)
                    return station;
            return null;
        }


        private void SortByNumber()
        {
            list.Sort(SortStation_Massive);
        }

        private static int SortStation_Massive(IStation station1, IStation station2)
        {
            if (station1.StationModel.Number == station2.StationModel.Number)
                return 0;
            var retval = station1.StationModel.Number.CompareTo(station2.StationModel.Number);
            return retval;
        }

        #endregion
    }
}