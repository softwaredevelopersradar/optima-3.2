﻿using System;
using System.Collections.Generic;
using GeoUtility.GeoSystem;
using Mapsui.Geometries;
using Mapsui.Projection;
using Mapsui.Providers;
using ModemControl;
using ModemControl.Interfaces;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.Enums;
using PropertyGridSettings.Model;
using PropertyGridStation.Model;
using WpfMapControl;
using YamlDotNet.Serialization;

namespace Optima_3_2
{
    public class EHS6 : IStation, IModelMethods<EHS6>
    {
        [YamlIgnore] private EHS6Model modemModelItem;

        #region Events IStation

        public event UpdateHandler OnUpdateModemModel = modemModelItem => { };

        #endregion

        #region Methods private

        private void CreateModemModel(int num, Location location)
        {
            modemModelItem = new EHS6Model();
            modemModelItem.Number = (byte)num;
            modemModelItem.Longitude = location.Longitude;
            modemModelItem.Latitude = location.Latitude;
        }

        #endregion

        #region Constructors

        public EHS6(EHS6Model model, ViewCoord viewCoord)
        {
            modemModelItem = model;
            model.ViewCoordField = viewCoord;

            SetCoordView(viewCoord);

            GSMIsueForStation =
                new GSMIsueForStation((byte)(model.Number - 1), modemModelItem.IPAddress, modemModelItem.Port);
        }

        public EHS6(ModemModel model, ViewCoord viewCoord)
        {
            modemModelItem = (EHS6Model)model;
            model.ViewCoordField = viewCoord;

            SetCoordView(viewCoord);

            GSMIsueForStation =
                new GSMIsueForStation((byte)(model.Number - 1), modemModelItem.IPAddress, modemModelItem.Port);
        }


        public EHS6(Location location, ViewCoord viewCoord, int num)
        {
            CreateModemModel(num, location);

            SetCoordView(viewCoord);

            GSMIsueForStation = new GSMIsueForStation((byte)(num - 1), modemModelItem.IPAddress, modemModelItem.Port);
        }

        #endregion

        #region Properties IStation

        [YamlIgnore] public IMapObject StationImage { get; set; }

        [YamlIgnore] public IFeature StationPolyLine { get; set; }

        [YamlIgnore] public IFeature[] SignalLines { get; set; } = new IFeature[2];

        [YamlIgnore] public int SignalLevel { get; set; } = -1;

        [YamlIgnore] public StationStatus StationStatus { get; set; } = StationStatus.Error;

        [YamlIgnore] public IGSMIsueForStation GSMIsueForStation { get; set; }

        public ModemModel StationModel
        {
            get => modemModelItem;
            set
            {
                modemModelItem = ((EHS6Model)value).Clone();
                GSMIsueForStation.SocketNumber = (byte)(value.Number - 1);
                GSMIsueForStation.IPAddress = modemModelItem.IPAddress;
                GSMIsueForStation.Port = modemModelItem.Port;
                OnUpdateModemModel(modemModelItem);
            }
        }

        #endregion

        #region Methods IStation

        public void SetCoordView(ViewCoord viewCoord)
        {
            modemModelItem.ViewCoordField = viewCoord;
            OnUpdateModemModel(modemModelItem);
        }


        public void UpdateModemModel()
        {
            OnUpdateModemModel(modemModelItem);
        }

        public void AllLedColor(Led color)
        {
            modemModelItem.GLONASS.L1 = color;
            modemModelItem.GLONASS.L2 = color;
            modemModelItem.GPS.L1 = color;
            modemModelItem.GPS.L2 = color;
            modemModelItem.GSM = color;
            modemModelItem.RadioF.F1 = color;
            modemModelItem.RadioF.F2 = color;
            modemModelItem.Radio = color;
            modemModelItem.RM = color;
            modemModelItem.GalileoAndBeidou.L1 = color;
            modemModelItem.GalileoAndBeidou.L2 = color;

            OnUpdateModemModel(modemModelItem);
        }

        public void LedsColor(bool GOLONAS_L1, bool GOLONAS_L2, bool GPS_L1, bool GPS_L2, bool GSM, bool RadioF_F1,
            bool RadioF_F2, bool Galileo_L1, bool Galileo_L2, bool Beidou_L1, bool Beidou_L2, bool Radio, bool RM,
            bool Existance_L1, bool Existance_L2)
        {
            Led color_L1;
            Led color_L2;

            if (Existance_L1)
                color_L1 = Led.Green;
            else color_L1 = Led.Yellow;

            if (Existance_L2)
                color_L2 = Led.Green;
            else color_L2 = Led.Yellow;


            modemModelItem.GLONASS.L1 = GOLONAS_L1 ? color_L1 : Led.Red;
            modemModelItem.GLONASS.L2 = GOLONAS_L2 ? color_L2 : Led.Red;
            modemModelItem.GPS.L1 = GPS_L1 ? color_L1 : Led.Red;
            modemModelItem.GPS.L2 = GPS_L2 ? color_L2 : Led.Red;
            modemModelItem.GalileoAndBeidou.L1 = Galileo_L1 ? color_L1 : Led.Red;
            modemModelItem.GalileoAndBeidou.L2 = Galileo_L2 ? color_L2 : Led.Red;
            modemModelItem.GSM = GSM ? Led.Green : Led.Red;
            modemModelItem.RadioF.F1 = RadioF_F1 ? Led.Green : Led.Red;
            modemModelItem.RadioF.F2 = RadioF_F2 ? Led.Green : Led.Red;
            modemModelItem.Radio = Radio ? Led.Green : Led.Red;
            modemModelItem.RM = RM ? Led.Green : Led.Red;

            OnUpdateModemModel(modemModelItem);
        }


        public Point[] DrawLines(Location firstPoint, Location secondPoint)
        {
            var uTM = new UTM();
            var uTM2 = new UTM();
            var errorstr = "";
            var keyValuePairs = new Dictionary<string, bool>();
            var str = secondPoint.ToUtm().Split(' ');
            UTM.TryParse(str[0].Substring(0, 2), str[0].Substring(2, 1), str[str.Length - 3].Split(',', '.')[0],
                str[str.Length - 1].Split(',', '.')[0], out uTM, out errorstr, out keyValuePairs);

            var str2 = firstPoint.ToUtm().Split(' ');
            UTM.TryParse(str2[0].Substring(0, 2), str2[0].Substring(2, 1), str2[str.Length - 3].Split(',', '.')[0],
                str2[str.Length - 1].Split(',', '.')[0], out uTM2, out errorstr, out keyValuePairs);

            var coordOfRange11 = new[]
            {
                new UTM(uTM.Zone, uTM.Band, uTM.East, uTM.North),
                new UTM(uTM2.Zone, uTM2.Band, uTM2.East, uTM2.North)
            };
            var len = Convert.ToUInt32(coordOfRange11.Length);
            var point = new Point[len];
            for (var j = 0; j < coordOfRange11.Length; j++)
                point[j] = Mercator.FromLonLat(((Geographic)coordOfRange11[j]).Longitude, ((Geographic)coordOfRange11[j]).Latitude);

            return point;
        }

        public Point[] DrawRange()
        {
            var coord = new Location(modemModelItem.Longitude, modemModelItem.Latitude);
            var uTM = new UTM();
            var errorstr = "";
            var keyValuePairs = new Dictionary<string, bool>();
            var str = coord.ToUtm().Split(' ');
            UTM.TryParse(str[0].Substring(0, 2), str[0].Substring(2, 1), str[str.Length - 3].Split(',', '.')[0],
                str[str.Length - 1].Split(',', '.')[0], out uTM, out errorstr, out keyValuePairs);

            var cosalph = Math.Round(Math.Cos(Math.PI / 180 * modemModelItem.Angle), 3);
            var sinalph = Math.Round(Math.Sin(Math.PI / 180 * modemModelItem.Angle), 3);
            var coordOfRange11 = new UTM[23];

            for (var i = 0; i < coordOfRange11.Length - 2; i++)
            {
                var ii = (double)i / 40;
                var X = modemModelItem.Distance / 3 * Math.Round(Math.Cos(2 * Math.PI * ii), 3);
                var Y = modemModelItem.Distance / 3 * Math.Round(Math.Sin(2 * Math.PI * ii), 3) +
                        2 * modemModelItem.Distance / 3;
                coordOfRange11[i] = new UTM(uTM.Zone, uTM.Band, uTM.East - (X * cosalph - Y * sinalph),
                    uTM.North + (Y * cosalph + X * sinalph));
            }

            coordOfRange11[coordOfRange11.Length - 2] = new UTM(uTM.Zone, uTM.Band,
                uTM.East + 1000 * cosalph - 2000 * sinalph, uTM.North - 2000 * cosalph - 1000 * sinalph);
            coordOfRange11[coordOfRange11.Length - 1] = new UTM(uTM.Zone, uTM.Band,
                uTM.East - 1000 * cosalph - 2000 * sinalph, uTM.North - 2000 * cosalph + 1000 * sinalph);


            var len = Convert.ToUInt32(coordOfRange11.Length);
            var point = new Point[len];
            for (var j = 0; j < coordOfRange11.Length; j++)
                point[j] = Mercator.FromLonLat(((Geographic)coordOfRange11[j]).Longitude,
                    ((Geographic)coordOfRange11[j]).Latitude);

            return point;
        }

        #endregion

        #region Methods IModelMethods IStation

        public bool EqualTo(IStation model)
        {
            return model.SignalLevel.Equals(SignalLevel)
                   || model.StationImage.Equals(StationImage)
                   || model.SignalLines.Equals(SignalLines)
                   || model.StationPolyLine.Equals(StationPolyLine)
                   || model.StationStatus.Equals(StationStatus)
                   || model.StationModel.Equals(StationModel);
        }

        IStation IModelMethods<IStation>.Clone()
        {
            return Clone();
        }

        public void Update(IStation model)
        {
            StationImage = model.StationImage;
            StationPolyLine = model.StationPolyLine;
            SignalLines = model.SignalLines;
            SignalLevel = model.SignalLevel;
            StationStatus = model.StationStatus;
            StationModel = model.StationModel;
        }

        #endregion

        #region Methods IModelMethods EHS6

        public bool EqualTo(EHS6 model)
        {
            return model.SignalLevel.Equals(SignalLevel)
                   || model.StationImage.Equals(StationImage)
                   || model.SignalLines.Equals(SignalLines)
                   || model.StationPolyLine.Equals(StationPolyLine)
                   || model.StationStatus.Equals(StationStatus)
                   || model.StationModel.Equals(StationModel)
                   || model.GSMIsueForStation.Equals(GSMIsueForStation);
        }

        public EHS6 Clone()
        {
            return new EHS6(modemModelItem, StationModel.ViewCoordField)
            {
                StationImage = StationImage,
                StationPolyLine = StationPolyLine,
                SignalLines = SignalLines,
                SignalLevel = SignalLevel,
                StationStatus = StationStatus,
                StationModel = StationModel,
                GSMIsueForStation = GSMIsueForStation
            };
        }

        public void Update(EHS6 model)
        {
            StationImage = model.StationImage;
            StationPolyLine = model.StationPolyLine;
            SignalLines = model.SignalLines;
            SignalLevel = model.SignalLevel;
            StationStatus = model.StationStatus;
            StationModel = model.StationModel;
            GSMIsueForStation = model.GSMIsueForStation;
        }

        #endregion
    }
}