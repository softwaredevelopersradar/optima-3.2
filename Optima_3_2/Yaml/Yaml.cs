﻿using System;
using System.IO;
using System.Text;
using PropertyGridSettings.Model;
using PropertyGridStation.Model;
using YamlDotNet.Serialization;

namespace Optima_3_2
{
    public static class Yaml
    {
        public static LocalProperties YamlLoad(string PathFile)
        {
            var text = "";
            try
            {
                using (var sr = new StreamReader(PathFile, Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var deserializer = new DeserializerBuilder().Build();

            var localProperties = new LocalProperties();
            try
            {
                localProperties = deserializer.Deserialize<LocalProperties>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return localProperties;
        }

        public static void YamlSaveLocalProperties(LocalProperties localProperties)
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(localProperties);

                using (var sw = new StreamWriter("LocalProperties.yaml", false, Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public static void YamlSaveStation(EHS6Model modemModel)
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(modemModel);

                using (var sw = new StreamWriter("Stations_" + modemModel.Number + ".yaml", false, Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public static void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (var sw = new StreamWriter(NameDotYaml, false, Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            var text = "";
            try
            {
                using (var sr = new StreamReader(NameDotYaml, Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return t;
        }
    }
}