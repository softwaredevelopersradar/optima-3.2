﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Optima_3_2.Converters
{
    public class DoubleBindingsVisible : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var value1 = System.Convert.ToBoolean(values[0]);
            var value2 = System.Convert.ToBoolean(values[1]);
            if (value1 || value2)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}