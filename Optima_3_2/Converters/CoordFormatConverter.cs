﻿using System;
using System.Globalization;
using System.Windows.Data;
using PropertyGridSettings.Model;

namespace Optima_3_2.Converters
{
    internal class CoordFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((ViewCoord)value == ViewCoord.Dd)
                return "DD";
            if ((ViewCoord)value == ViewCoord.DMm)
                return "DD_MM_mm";
            if ((ViewCoord)value == ViewCoord.DMSs)
                return "DD_MM_SS_ss";
            return "DD_dd";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}