﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using PropertyGridSettings.Model;

namespace Optima_3_2.Converters
{
    public class AdminVisibleOnlyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((AccessTypes)value == AccessTypes.Admin)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}