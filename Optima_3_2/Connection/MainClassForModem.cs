﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Client_New;
using Client_New.Events;
using ModemControl;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.Connection;
using Optima_3_2.Enums;
using ProtocolOptima;
using WPFControlConnection;
using WpfMapControl;
using Color = Mapsui.Styles.Color;
using CommandType = Optima_3_2.Enums.CommandType;
using Point = Mapsui.Geometries.Point;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        private GSMConnection gSMConnection;
        private RadioConnection radioConnection;

        private int ASCSignalQuality = 0;


        private Brush readColorBrush = Brushes.LightGreen;
        private Brush writeColorBrush = Brushes.White;
        private Brush commentColorBrush = Brushes.DarkOrange;
        private Brush errorColorBrush = Brushes.Red;
        private Brush readCommandColorBrush = Brushes.OrangeRed;


        private void InitializeGSMConnection()
        {
            gSMConnection = new GSMConnection(propertiesPropGrid.Local.Common.ComPort_GSM, "127.0.0.1");

            gSMConnection.OnCloseComPort += COMPort_OnClose;

            gSMConnection.OnClientRegistered += GSM_OnClientRegistered;
            gSMConnection.OnCloseSocket += GSM_OnCloseSocket;
            gSMConnection.OnCodegrammReceived += GSM_OnCodegrammReceived;
            gSMConnection.OnCodegramSended += GSM_OnCodegramSended;
            gSMConnection.OnConfirmedSendCodegram += GSM_OnConfirmedSendCodegram;
            gSMConnection.OnConnectedToServer += GSM_OnConnectedToServer;
            gSMConnection.OnConnectionError += GSM_OnConnectionError;
            gSMConnection.OnCyclePollStarted += GSM_OnCyclePollStarted;
            gSMConnection.OnEndOperation += GSM_OnEndOperation;
            gSMConnection.OnCOMPortChanged += GSM_OnGSMCOMPortChanged;
            gSMConnection.OnModemFind += GSM_OnModemFind;
            gSMConnection.OnRead += GSM_OnRead;
            gSMConnection.OnResetGSMFinished += GSM_OnResetGSMFinished;
            gSMConnection.OnSocketChecked += GSM_OnSocketChecked;
            gSMConnection.OnSomeErrorInitSocket += GSM_OnSomeErrorInitSocket;
            gSMConnection.OnSomeMessageNotSended += GSM_OnSomeMessageNotSended;
            gSMConnection.OnSomeSocketInitialized += GSM_OnSomeSocketInitialized;
            gSMConnection.OnStartOperation += GSM_OnStartOperation;
            gSMConnection.OnWriteCodegram += GSM_OnWriteCodegram;
            gSMConnection.OnWriteCommand += GSM_OnWriteCommand;
            gSMConnection.OnSignalQuality += GSM_OnSignalQuality;

            InitializeLogFile();
        }

        private void COMPort_OnClose(object sender, EventArgs e)
        {
            EnableGSMInMainThread(false);
        }

        private void InitializationRadio()
        {
            radioConnection = new RadioConnection(propertiesPropGrid.Local.Common.ComPort_Radio);
        }

        private void GSM_OnWriteCommand(object sender, StringEventArgs e)
        {
            Dispatcher.Invoke(() => { ModemControlConnection.ShowWrite(); });
            AddStringToLoginMainThread("w:  " + e.Data.Trim('\n', '\r'), writeColorBrush); //, true); // помечает сообщения которые не будут отображаться при не отмеченом в логе чекбоксе 
        }

        private void GSM_OnWriteCodegram(object sender, BytesEventArgs e)
        {
            Dispatcher.Invoke(() => { ModemControlConnection.ShowWrite(); });
            AddStringToLoginMainThread("w:  " + BitConverter.ToString(e.Data), writeColorBrush);
        }

        private void GSM_OnSignalQuality(object sender, IntegerEventArgs e)
        {
            ASCSignalQuality = e.Data;

            AddStringToLoginMainThread((SignalType)sender + " ~~~~ Signal level: " + ASCSignalQuality + " ~~~~ \n", commentColorBrush);
        }

        private void GSM_OnRead(object sender, StringEventArgs e)
        {
            Dispatcher.Invoke(() => { ModemControlConnection.ShowRead(); });

            if (e.Data.Contains("SISR"))
            {
                var str = e.Data.Split(',');
                if (Convert.ToInt32(str[1]) <= 10)
                    AddStringToLoginMainThread(" ~~~~ Try read data from socket " + str[1].Trim('\n', '\r') + " ~~~~ \n", readCommandColorBrush);
            }
            else if (e.Data.Contains("\u0003\u0002\u0001"))
            {
                var bytes = Encoding.ASCII.GetBytes(e.Data);
                AddStringToLoginMainThread("r: " + BitConverter.ToString(bytes), readColorBrush);
                return;
            }

            AddStringToLoginMainThread("r:  " + e.Data.Trim('\n', '\r'), readColorBrush); //, true); // помечает сообщения которые не будут отображаться при не отмеченом в логе чекбоксе 
        }


        private void GSM_OnSomeSocketInitialized(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ End initialization of socket " + (e.Data - 1) + " ~~~~ \n", commentColorBrush);

            var index = e.Data;
            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    stationMainThread.StationModel.State = "Connecting...";
                    stationMainThread.UpdateModemModel();
                }
            });
        }

        private void GSM_OnSomeMessageNotSended(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~  Timed out message on socket " + (e.Data - 1) + " ~~~~ \n", errorColorBrush);

            var index = e.Data;
            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    //stationMainThread.StationModel.State = "No answer";
                    stationMainThread.UpdateModemModel();
                }
            });
        }

        private void GSM_OnSomeErrorInitSocket(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Initialization error on socket " + (e.Data - 1) + " ~~~~ \n", errorColorBrush);

            var index = e.Data;
            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    //stationMainThread.StationModel.State = "Error initialization";
                    stationMainThread.UpdateModemModel();
                }
            });
        }

        private void GSM_OnSocketChecked(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Socket " + (e.Data - 1) + " checked " + " ~~~~ \n", commentColorBrush);
        }

        private void GSM_OnResetGSMFinished(object sender, EventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ GSM connection ready to work ~~~~ \n", commentColorBrush);
            EnableGSMFixInMainThread(true);
            EnableGSMInMainThread(true);
        }

        private void GSM_OnGSMCOMPortChanged(object sender, EventArgs e)
        {
        }

        private void GSM_OnCyclePollStarted(object sender, EventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Start cycle poll ~~~~ \n", commentColorBrush);
        }

        private void GSM_OnEndOperation(object sender, EventArgs e)
        {
            EnableGSMInMainThread(true);
        }

        private void GSM_OnStartOperation(object sender, EventArgs e)
        {
            EnableGSMInMainThread(false);
        }

        private void GSM_OnConnectionError(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Connection error on socket " + (e.Data - 1) + " ~~~~ \n", errorColorBrush);

            var index = e.Data;
            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    stationMainThread.StationModel.State = "Disconnected";
                    stationMainThread.UpdateModemModel();
                    stationMainThread.GSMIsueForStation.IsInitialized = false;
                    EmptyStationsState(stationMainThread);
                }
            });
        }

        private void GSM_OnConnectedToServer(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Socket " + (e.Data - 1) + " connected to the transmitter" + " ~~~~ \n", commentColorBrush);

            var index = e.Data;
            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    DrawIndicatorOfSignalLevel(stationMainThread, 0);
                    stationMainThread.StationModel.State = "Connected";
                    stationMainThread.GSMIsueForStation.IsInitialized = true;
                    stationMainThread.UpdateModemModel();
                }
            });
        }

        private void GSM_OnConfirmedSendCodegram(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Confirmed send codegram on socket " + (e.Data - 1) + " ~~~~ \n", commentColorBrush);
        }

        private void GSM_OnCodegramSended(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Codegram Sended on socket " + (e.Data - 1) + " ~~~~ \n", commentColorBrush);

            var index = e.Data;

            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    stationMainThread.GSMIsueForStation.IsFirstMessageSent = true;
                    stationMainThread.StationModel.State = "Wait answer...";
                    stationMainThread.UpdateModemModel();
                }
            });
        }

        private void GSM_OnCodegrammReceived(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Codegram decode from socket " + (e.Data - 1) + " ~~~~ \n", readCommandColorBrush);

            var index = e.Data;

            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    CodegramRecievedAct(stationMainThread, (StationCodegram)sender, true);
                    stationMainThread.StationModel.State = "Connected, answer received";
                    stationMainThread.UpdateModemModel();
                }
            });
        }

        private void GSM_OnCloseSocket(object sender, IntegerEventArgs e)
        {
            AddStringToLoginMainThread(" ~~~~ Socket " + (e.Data - 1) + " close ~~~~ \n", commentColorBrush);
            var index = e.Data;

            Dispatcher.Invoke(() =>
            {
                var stationMainThread = stationsList[index];
                if (stationMainThread != null)
                {
                    stationMainThread.StationModel.State = "Disconnected";
                    stationMainThread.UpdateModemModel();
                    stationMainThread.GSMIsueForStation.IsInitialized = false;
                    stationMainThread.GSMIsueForStation.IsFirstMessageSent = false;
                    EmptyStationsState(stationsList[index]);
                }
            });
        }

        private void GSM_OnModemFind(object sender, EventArgs e)
        {
            Dispatcher.Invoke(() => { ModemControlConnection.ServerConnectionState = ConnectionStates.Connected; });
            AddStringToLoginMainThread(" ~~~~ Establishing a GSM connection ~~~~ \n", commentColorBrush);
        }

        private void GSM_OnClientRegistered(object sender, EventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                ResetGSMToggleButton.IsEnabled = true;
                EnableGSM(true);

                foreach (var station in stationsList) EmptyStationsState(station);
            });
        }

        private bool IsOneStationSelected()
        {
            for (var i = 0; i < stationsList.Count; i++)
                if (Table.ListModemModel[i].IsActive)
                    return true;
            return false;
        }


        #region Click region

        private void SendSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            if (IsOneStationSelected() == false)
            {
                MessageBox.Show("Select at least one station");
                return;
            }

            if (Radio.IsChecked == true)
                radioConnection.SendCommand(new ListGSMConnection(stationsList, false),
                    (CommandType)TypeOfCommand.SelectedItem, GPSL1L2.IsToggleButtonChecked,
                    GLONASSL1L2.IsToggleButtonChecked, Galileo_BeidouL1L2.IsToggleButtonChecked,
                    switchFreqButton.SelectedIndex + 1);
            else if (GSM.IsChecked == true)
                gSMConnection.SendCommand(new ListGSMConnection(stationsList, false),
                    (CommandType)TypeOfCommand.SelectedItem, GPSL1L2.IsToggleButtonChecked,
                    GLONASSL1L2.IsToggleButtonChecked, Galileo_BeidouL1L2.IsToggleButtonChecked,
                    switchFreqButton.SelectedIndex + 1);
        }

        private void SendAllButton_Click(object sender, RoutedEventArgs e)
        {
            if (Radio.IsChecked == true)
                radioConnection.SendCommand(new ListGSMConnection(stationsList),
                    (CommandType)TypeOfCommand.SelectedItem, GPSL1L2.IsToggleButtonChecked,
                    GLONASSL1L2.IsToggleButtonChecked, Galileo_BeidouL1L2.IsToggleButtonChecked,
                    switchFreqButton.SelectedIndex + 1);
            else if (GSM.IsChecked == true)
                gSMConnection.SendCommand(new ListGSMConnection(stationsList), 
                    (CommandType)TypeOfCommand.SelectedItem, GPSL1L2.IsToggleButtonChecked, 
                    GLONASSL1L2.IsToggleButtonChecked, Galileo_BeidouL1L2.IsToggleButtonChecked,
                    switchFreqButton.SelectedIndex + 1);
        }

        #endregion

        #region Interface actions

        private void EnableGSMFix(bool enable)
        {
            ResetGSMToggleButton.IsEnabled = enable;
        }

        private void EnableGSM(bool enable)
        {
            GSM.IsEnabled = enable;
            EnableSend(enable);
        }

        private void EnableSend(bool enable)
        {
            SendSelectedButton.IsEnabled = enable;
            SendAllButton.IsEnabled = enable;
            SendCyclePollButton.IsEnabled = enable;
        }

        private void EnsbleSendWithoutCycleButton(bool enable)
        {
            SendSelectedButton.IsEnabled = enable;
            SendAllButton.IsEnabled = enable;
        }

        private void EnableGSMFixInMainThread(bool enable)
        {
            Dispatcher.Invoke(() => { ResetGSMToggleButton.IsEnabled = enable; });
        }

        private void EnableSendInMainThread(bool enable)
        {
            Dispatcher.Invoke(() => { EnableSend(enable); });
        }

        private void EnsbleSendWithoutCycleButtonInMainThread(bool enable)
        {
            Dispatcher.Invoke(() => { EnsbleSendWithoutCycleButton(enable); });
        }


        private void EnableGSMInMainThread(bool enable)
        {
            Dispatcher.Invoke(() => { EnableGSM(enable); });
        }



        #endregion

        #region Visualisation station

        private void DrawIndicatorOfSignalLevel(IStation station, int level)
        {
            if (RasterMap.mapControl.IsMapLoaded)
            {
                DeleteALLSignalLines(station);

                station.SignalLevel = level;

                ChoseColor_IndicatorOfSignalLevel(station, level);
            }
        }


        private void ChoseColor_IndicatorOfSignalLevel(IStation station, int level)
        {
            if(level == -1)
                return;
            var mainStationObj = FindMainStation();

            var LongittudeMain = mainStationObj.StationModel.Longitude;
            var LatitudeMain = mainStationObj.StationModel.Latitude;
            var LongittudeStation = station.StationModel.Longitude;
            var LatitudeStation = station.StationModel.Latitude;

            Location middLocation = new Location((LongittudeMain + LongittudeStation) / 2, (LatitudeMain + LatitudeStation) / 2);

            var lineFromMainToMidd = station.DrawLines(new Location(LongittudeMain, LatitudeMain), middLocation);
            var lintFromMiddToStation  = station.DrawLines(middLocation, new Location(LongittudeStation, LatitudeStation));

            if (level <= 5)
                station.SignalLines[0] = RasterMap.mapControl.AddPolyline(lintFromMiddToStation, Color.FromArgb(255, 255, 51 * level, 0));
            else station.SignalLines[0] = RasterMap.mapControl.AddPolyline(lintFromMiddToStation, Color.FromArgb(255, (-51 * level) + 510, 255, 0)); 

            if (ASCSignalQuality <= 5)
                station.SignalLines[1] = RasterMap.mapControl.AddPolyline(lineFromMainToMidd, Color.FromArgb(255, 255, 51 * ASCSignalQuality, 0));
            else station.SignalLines[1] = RasterMap.mapControl.AddPolyline(lineFromMainToMidd, Color.FromArgb(255, (-51 * ASCSignalQuality) + 510, 255, 0));
        }

        private void DrawIndicationOfConnectionState(IStation station, StationStatus stationStatus)
        {
            if (RasterMap.mapControl.IsMapLoaded)
            {
                RasterMap.mapControl.RemoveObject(station?.StationPolyLine);

                station.StationStatus = stationStatus;

                ChoseColor_IndicationOfConnectionState(station, stationStatus);
            }
        }

        private void ChoseColor_IndicationOfConnectionState(IStation station, StationStatus stationStatus)
        {
            switch (stationStatus)
            {
                case StationStatus.Normal:
                    station.StationPolyLine =
                        RasterMap.mapControl.AddPolygon(station.DrawRange(),
                            Color.FromArgb(100, 87, 175, 254));
                    break;
                case StationStatus.WithoutRadiation:
                    station.StationPolyLine =
                        RasterMap.mapControl.AddPolygon(station.DrawRange(),
                            Color.FromArgb(100, 255, 250, 87));
                    break;
                case StationStatus.Error:
                    station.StationPolyLine =
                        RasterMap.mapControl.AddPolygon(station.DrawRange(),
                            Color.FromArgb(100, 130, 130, 130));
                    break;
                default:
                    station.StationPolyLine =
                        RasterMap.mapControl.AddPolygon(station.DrawRange(),
                            Color.FromArgb(100, 135, 135, 135));
                    break;
            }
        }

        private void ErrorStationsState(IStation station)
        {
            station.AllLedColor(Led.Gray);
            DrawIndicationOfConnectionState(station, StationStatus.Error);
            DrawIndicatorOfSignalLevel(station, 0);
        }


        private void EmptyStationsState(IStation station)
        {
            station.AllLedColor(Led.Gray);
            DrawIndicationOfConnectionState(station, StationStatus.Error);
            DeleteALLSignalLines(station);
        }


        private void CodegramRecievedAct(IStation station, StationCodegram codegram, bool thisIsGSM)
        {
            station.LedsColor(codegram.GLONASS_L1, codegram.GLONASS_L2, codegram.GPS_L1, codegram.GPS_L2, thisIsGSM,
                codegram.FreqPC == 1 ? true : false,
                codegram.FreqPC == 2 ? true : false,
                codegram.Galileo_L1, codegram.Galileo_L2, codegram.Beidou_L1, codegram.Beidou_L2, !thisIsGSM, true, codegram.Existance_L1,
                codegram.Existance_L2);

            DrawIndicatorOfSignalLevel(station, 9);//codegram.SignalLevel);

            if (codegram.Galileo_L1 == false && codegram.Galileo_L2 == false && codegram.GLONASS_L1 == false && codegram.GLONASS_L2 == false
                && codegram.GPS_L1 == false && codegram.GPS_L2 == false && codegram.Beidou_L1 == false && codegram.Beidou_L2 == false)
                DrawIndicationOfConnectionState(station, StationStatus.Error);
            else if (codegram.Existance_L1 || codegram.Existance_L2)
                DrawIndicationOfConnectionState(station, StationStatus.Normal);
            else if (codegram.Existance_L1 == false || codegram.Existance_L2 == false)
                DrawIndicationOfConnectionState(station, StationStatus.WithoutRadiation);
        }

        #endregion

        #region Diagnostic Hendlers

        private void AllSocketCheckLOG_Click(object sender, RoutedEventArgs e)
        {
            gSMConnection.AllSocketDiagnostic();
        }

        private void CheckModemLOG_atv_Click(object sender, RoutedEventArgs e)
        {
            gSMConnection.ModemDiagnosticATV();
        }

        private void CheckModemLOG_scfg_Click(object sender, RoutedEventArgs e)
        {
            gSMConnection.ModemDiagnosticSCFG();
        }

        private void CheckSignalQuality_Click(object sender, RoutedEventArgs e)
        {
            gSMConnection.SignalQualityPoll();
        }
        #endregion
    }
}