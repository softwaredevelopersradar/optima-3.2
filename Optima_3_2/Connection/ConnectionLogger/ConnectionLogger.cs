﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        private bool isWriteToLog;

        private void InitializeLogFile()
        {
            var dataTime = DateTime.Now;
            string filename = $"Log_Day{dataTime.Day}_Month{dataTime.Month}_Year{dataTime.Year}_Hour{dataTime.Hour}_Minute{dataTime.Minute}.txt";
            string folder =  $@"{Environment.CurrentDirectory}\Log";

            var directoryInfo = new DirectoryInfo(folder);

            if (!directoryInfo.Exists) 
                directoryInfo.Create();

            Log.NameOfTextFile = filename;
            Log.PathToTextFile = folder;

            //try
            //{
            //    _fstream = new StreamWriter(path, false, Encoding.Default);
            //}
            //catch (FileNotFoundException)
            //{
            //    Console.WriteLine("Invalid path to text file (Log): ");
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine($"Error related with file (Log): {ex}");
            //}

            ////

        }

        public void CloseConnectionLogger()
        {
            Log.CloseLogFile();
        }

        private void AddStringToLoginMainThread(string str, Brush brush, bool isMarkedAsByte = false)
        {
            if (isWriteToLog)
                Dispatcher.Invoke(() => { Log.AddTextToLog(str, brush, isMarkedAsByte); });
            else Log.AddTextToFile($"{DateTime.Now.TimeOfDay}>   {str}");
        }
    }
}