﻿using System;
using Optima_3_2.ClassesForStation.Stations.List;

namespace Optima_3_2.Connection.Interfaces
{
    public interface IResetConnection
    {
        #region Events

        EventHandler OnResetGSMFinished { get; set; }

        #endregion

        #region Functions

        void ResetAllGSMSockets(StationsList stationsList);

        #endregion
    }
}