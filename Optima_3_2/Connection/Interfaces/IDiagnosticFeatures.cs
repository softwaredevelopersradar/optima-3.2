﻿namespace Optima_3_2.Connection.Interfaces
{
    public interface IDiagnosticFeatures
    {
        void AllSocketDiagnostic();

        void ModemDiagnosticATV();

        void ModemDiagnosticSCFG();
    }
}