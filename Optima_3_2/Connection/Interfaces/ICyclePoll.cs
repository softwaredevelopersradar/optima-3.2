﻿using System;
using Optima_3_2.ClassesForStation.Stations.List;

namespace Optima_3_2.Connection.Interfaces
{
    public interface ICyclePoll
    {
        #region Events

        EventHandler OnCyclePollStarted { get; set; }
        EventHandler OnyclePollEnded { get; set; }

        #endregion

        #region Functions

        void StopCyclePoll();

        void StartCyclePoll(StationsList stationsList, int period);

        #endregion
    }
}