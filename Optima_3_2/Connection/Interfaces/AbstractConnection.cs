﻿using System;
using Client_New;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.Enums;

namespace Optima_3_2.Connection
{
    public abstract class AbstractConnection
    {
        #region Events

        public EventHandler<StringEventArgs> OnCOMPortChanged;

        #endregion

        #region Properties

        public abstract string ComPort { get; set; }

        #endregion


        #region Functions

        public abstract void EndWork();

        #endregion


        #region Send

        private bool GPSL1L2;
        private bool GLONASSL1L2;
        private bool Galileo_BeidouL1L2;
        private byte FreqPC = 1;
        private CommandType mainComandType = CommandType.POLL;

        protected virtual void CreateMessage(CommandType _mainComandType, bool gPSL1L2, bool gLONASSL1L2,
            bool galileo_BeidouL1L2, int freqPC)
        {
            GPSL1L2 = gPSL1L2;
            GLONASSL1L2 = gLONASSL1L2;
            Galileo_BeidouL1L2 = galileo_BeidouL1L2;
            FreqPC = (byte)freqPC;
            mainComandType = _mainComandType;
        }

        protected virtual void Send(int socketNum, Client client, string ipAddres)
        {
            var num = (byte)socketNum;

            switch (mainComandType)
            {
                case CommandType.POLL:
                    client.SendPoll(num, ipAddres);
                    break;
                case CommandType.ON:
                    client.SendOnOff(num, ipAddres, ProtocolOptima.CommandType.ON, GPSL1L2, GPSL1L2, GLONASSL1L2,
                        GLONASSL1L2, Galileo_BeidouL1L2, Galileo_BeidouL1L2, Galileo_BeidouL1L2, Galileo_BeidouL1L2,
                        FreqPC);
                    break;
                case CommandType.OFF:
                    client.SendOnOff(num, ipAddres, ProtocolOptima.CommandType.OFF, GPSL1L2, GPSL1L2, GLONASSL1L2,
                        GLONASSL1L2, Galileo_BeidouL1L2, Galileo_BeidouL1L2, Galileo_BeidouL1L2, Galileo_BeidouL1L2,
                        FreqPC);
                    break;
            }
        }


        public abstract void SendCommand(IListGSMConnection gSMConnections, CommandType commandType, bool GPSL1L2,
            bool GLONASSL1L2, bool GalileoAndBeidouL1L2, int FrequencyPC);

        #endregion
    }
}