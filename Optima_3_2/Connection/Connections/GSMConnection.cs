﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using Client_New;
using Client_New.Events;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.ClassesForStation.Stations.List;
using Optima_3_2.Connection.Interfaces;
using ProtocolOptima;
using CommandType = Optima_3_2.Enums.CommandType;

namespace Optima_3_2.Connection
{
    public class GSMConnection : AbstractConnection, ICyclePoll, IResetConnection, IDiagnosticFeatures
    {
        private static readonly EventWaitHandle ready = new AutoResetEvent(false);
        private static readonly EventWaitHandle go = new AutoResetEvent(false);
        private static readonly EventWaitHandle send = new AutoResetEvent(false);
        private static readonly EventWaitHandle init = new AutoResetEvent(false);
        private static readonly EventWaitHandle check = new AutoResetEvent(false);
        private static readonly EventWaitHandle close = new AutoResetEvent(false);
        private static readonly EventWaitHandle read = new AutoResetEvent(false);
        private static readonly EventWaitHandle readall = new AutoResetEvent(false);

        private readonly GSMClient gSMclient = new GSMClient();
        private readonly IListGSMConnection socetsForQuery = new ListGSMConnection();
        private int amountOfChecked = 2;
        private int amountOfResend = 2;

        private string comPortGSM = "COM1";
        private bool isWeCanReadCodegram = true;


        public EventHandler OnStartOperation;
        public EventHandler OnClientRegistered;
        public EventHandler OnCloseComPort;
        public EventHandler<IntegerEventArgs> OnSocketChecked;
        public EventHandler<IntegerEventArgs> OnCloseSocket;
        public EventHandler<IntegerEventArgs> OnCodegrammReceived;
        public EventHandler<IntegerEventArgs> OnCodegramSended;
        public EventHandler<IntegerEventArgs> OnConfirmedSendCodegram;
        public EventHandler<IntegerEventArgs> OnConnectedToServer;
        public EventHandler<IntegerEventArgs> OnConnectionError;
        public EventHandler<IntegerEventArgs> OnSignalQuality;
        public EventHandler<IntegerEventArgs> OnSomeErrorInitSocket;
        public EventHandler<IntegerEventArgs> OnSomeMessageNotSended;
        public EventHandler<IntegerEventArgs> OnSomeSocketInitialized;
        public EventHandler<BytesEventArgs> OnWriteCodegram;
        public EventHandler<StringEventArgs> OnWriteCommand;
        public EventHandler<StringEventArgs> OnRead;
        public EventHandler OnEndOperation;
        public EventHandler OnModemFind;

        private int permittedAmountOfReinitialization = 1;
        private Thread sendThread;
        private IListGSMConnection socketsForBuffer = new ListGSMConnection();

        private int waitingTimeAfterConnectingBeforeSend = 16000;
        private int waitingTimeForCheckingSocket = 30000;
        private int waitingTimeForInitialization = 38000;
        private int waitingTimeForResendIfCheckingGood = 4000;


        #region Propeties

        public override string ComPort
        {
            get => comPortGSM;
            set
            {
                comPortGSM = value;
                gSMclient.OpenPort(comPortGSM, 9600, Parity.None, 8, StopBits.One, ClientType.GSM);
                gSMclient.SendStartCommand();
            }
        }

        public int AmountOfChecked
        {
            get => amountOfChecked;
            set
            {
                if (amountOfChecked == value)
                    return;
                amountOfChecked = value;
            }
        }


        public int AmountOfResend
        {
            get => amountOfResend;
            set
            {
                if (amountOfResend == value)
                    return;
                amountOfResend = value;
            }
        }

        public int PermittedAmountOfReinitialization
        {
            get => permittedAmountOfReinitialization;
            set
            {
                if (permittedAmountOfReinitialization == value)
                    return;
                permittedAmountOfReinitialization = value;
            }
        }

        public int WaitingTimeAfterConnectingBeforeSend
        {
            get => waitingTimeAfterConnectingBeforeSend;
            set
            {
                if (waitingTimeAfterConnectingBeforeSend == value)
                    return;
                waitingTimeAfterConnectingBeforeSend = value;
            }
        }

        public int WaitingTimeForCheckingSocket
        {
            get => waitingTimeForCheckingSocket;
            set
            {
                if (waitingTimeForCheckingSocket == value)
                    return;
                waitingTimeForCheckingSocket = value;
            }
        }

        public int WaitingTimeForInitialization
        {
            get => waitingTimeForInitialization;
            set
            {
                if (waitingTimeForInitialization == value)
                    return;
                waitingTimeForInitialization = value;
            }
        }

        public int WaitingTimeForResendIfCheckingGood
        {
            get => waitingTimeForResendIfCheckingGood;
            set
            {
                if (waitingTimeForResendIfCheckingGood == value)
                    return;
                waitingTimeForResendIfCheckingGood = value;
            }
        }

        public string LocalIP
        {
            get => gSMclient.LocalIP;
            set => gSMclient.LocalIP = value;
        }

        #endregion


        #region Initialize Features

        public GSMConnection(string comPortGSM, string iPAdressControlStation)
        {
            gSMclient = new GSMClient();

            gSMclient.LocalIP = iPAdressControlStation;

            gSMclient.OnSignalQuality += GSMClient_OnSignalQuality;

            gSMclient.OnSocketInitialized += GSMClient_OnSocketInitialized;
            gSMclient.OnSocketConnected += GSMClient_ConnectedToServer;
            gSMclient.OnClientRegistered += GSMClient_ClientRegistered;
            gSMclient.OnCodegramRecieved += GSMClient_CodegrammReceived;
            gSMclient.OnCodegramSended += GSMClient_OnCodegramSended;
            gSMclient.OnFindModem += GSMClient_OnFindModem;
            gSMclient.OnConfirmedSendCodegram += GSMClient_OnConfirmedSendCodegram;
            gSMclient.OnSomeMessageNotSended += GSMClient_OnSomeMessageNotSended;
            gSMclient.OnSomeMessageRecived += GSMClient_OnSomeMessageRecived;
            gSMclient.OnSocketRecivedBufferClear += GSMClient_OnSocketRecivedBufferClear;
            gSMclient.OnCodegramDecode += GSMClient_OnCodegramDecoded;
            gSMclient.OnSocketChecked += GSMClient_OnSocketChecked;
            gSMclient.OnSocketClosed += GSMclient_OnSocketClosed;
            gSMclient.OnSocketClear += GSMClient_OnSocketClear;
            gSMclient.OnReadyToSendCodegram += GSMClient_OnErrorClose;

            gSMclient.OnSomeErrorInitSocket += GSMClient_OnSomeErrorInitSocket;
            gSMclient.OnSomeErrorWrite += GSMClient_OnSomeErrorWrite;
            gSMclient.OnErrorOcured += GSMClient_ConnectionError;
            gSMclient.OnErrorClear += GSMClient_OnErrorClear;
            gSMclient.OnClientCheck += GSMClient_OnClientCheck;
            gSMclient.OnErrorOcured += GSMClient_OnErrorClose;
            gSMclient.OnErrorClose += GSMClient_OnErrorClose;
            gSMclient.OnSomeErrorInitClient += GSMClient_OnSomeErrorInitClient;
            gSMclient.OnSomeErrorRead += GSMClient_OnSomeErrorInitClient;

            gSMclient.OnWriteString += OnWriteComand;
            gSMclient.OnWriteByte += OnWriteCodeg;
            gSMclient.OnReadString += OnReadComand;
            gSMclient.OnClosePort += OnClosePort;
            gSMclient.AmountOfTrySendCommand = 5;

            ComPort = comPortGSM;

            sendThread = new Thread(StartSendThread);
            sendThread.Start();
        }


        private void OnClosePort(object sender, EventArgs e)
        {
            OnCloseComPort?.Invoke(sender, e);
        }

        private void GSMClient_OnSignalQuality(object sender, IntegerEventArgs e)
        {
            OnSignalQuality?.Invoke(sender, e);
        }

        private void OnReadComand(object sender, StringEventArgs e)
        {
            OnRead?.Invoke(sender, e);
        }

        private void OnWriteCodeg(object sender, BytesEventArgs e)
        {
            OnWriteCodegram?.Invoke(sender, e);
        }

        private void OnWriteComand(object sender, StringEventArgs e)
        {
            if (OnWriteCommand != null)
                OnWriteCommand?.Invoke(sender, e);
        }

        #endregion


        #region GSM Events handlers

        private readonly List<byte> socketsWithMessages = new List<byte>();

        private void GSMClient_OnSomeMessageRecived(object sender, ByteEventArgs e)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(e.Data);

                if (station != null)
                {
                    if (isWeCanReadCodegram)
                    {
                        isWeCanReadCodegram = false;
                        station.IsIgnore = true;
                        GSMHandlerRead(e.Data);
                    }
                    else
                    {
                        station.IsSomeMessageReceived = true;
                    }
                }
                else
                {
                    if (isWeCanReadCodegram)
                        GSMHandlerRead(e.Data);
                    else socketsWithMessages.Add(e.Data);
                }
            }
        }


        private void GSMClient_OnCodegramDecoded(object sender, ByteEventArgs e)
        {
            lock (socetsForQuery)
            {
                isWeCanReadCodegram = true;
                var station = socetsForQuery.GetBySocketNumber(e.Data);

                if (station != null)
                {
                    station.IsSomeMessagesReaded = true;
                }
                else
                {
                    isWeCanReadCodegram = false;
                    GSMHandlerRead(socketsWithMessages[0]);
                    socketsWithMessages.RemoveAt(0);
                    return;
                }


                for (var i = 0; i < socetsForQuery.Count; i++)
                    if (socetsForQuery[i].IsSomeMessageReceived)
                    {
                        isWeCanReadCodegram = false;
                        GSMHandlerRead(socetsForQuery[i].SocketNumber);
                        socetsForQuery[i].IsIgnore = true;
                        socetsForQuery[i].IsSomeMessageReceived = false;
                        break;
                    }
            }
        }

        private void GSMClient_OnSocketRecivedBufferClear(object sender, ByteEventArgs e)
        {
            readall.Set();
        }


        private void GSMClient_OnSomeMessageNotSended(object sender, ByteEventArgs e)
        {
            SomeMessageNotSended(e.Data);

            OnSomeMessageNotSended?.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            ready.Set();
        }


        private void GSMclient_OnSocketClosed(object sender, ByteEventArgs e)
        {
            CloseSocket(e.Data);

            OnCloseSocket?.Invoke(sender, new IntegerEventArgs(e.Data + 1));
        }

        private void GSMClient_OnSocketClear(object sender, ByteEventArgs e)
        {
            close.Set();
        }

        private void GSMClient_OnErrorClear(object sender, ByteEventArgs e)
        {
            //close.Set();
        }

        private void GSMClient_OnSomeErrorWrite(object sender, StringEventArgs e)
        {
        }

        private void GSMClient_OnSomeErrorInitSocket(object sender, ByteEventArgs e)
        {
            OnSomeErrorInitSocket?.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            RestartThreadForInitialization();
        }

        private void GSMClient_CodegrammReceived(object sender, ByteEventArgs e)
        {
            var codegram = (StationCodegram)sender;
            CodegrammReceived(e.Data);

            OnCodegrammReceived?.Invoke(codegram, new IntegerEventArgs(e.Data + 1));
            read.Set();
        }

        private void GSMClient_OnSocketChecked(object sender, ByteEventArgs e)
        {
            OnSocketChecked?.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            RestartThreadForChecked();
        }

        private void GSMClient_OnCodegramSended(object sender, ByteEventArgs e)
        {
            OnCodegramSended?.Invoke(sender, new IntegerEventArgs(e.Data + 1));
        }

        private void GSMClient_OnConfirmedSendCodegram(object sender, ByteEventArgs e)
        {
            OnConfirmedSendCodegram?.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            RestartThreadForSend();
        }

        private void GSMClient_ConnectionError(object sender, ByteEventArgs e)
        {
            OnConnectionError?.Invoke(send, new IntegerEventArgs(e.Data + 1));

            ConnectionError(e.Data);

            ready.Set();
        }

        private void GSMClient_ConnectedToServer(object sender, ByteEventArgs e)
        {
            ConnectedToServer(e.Data);

            OnConnectedToServer?.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            ready.Set();
        }

        private void GSMClient_OnSocketInitialized(object sender, ByteEventArgs e)
        {
            RestartThreadForInitialization();

            OnSomeSocketInitialized?.Invoke(sender, new IntegerEventArgs(e.Data + 1));
        }

        private void GSMClient_ClientRegistered()
        {
            OnClientRegistered?.Invoke(new object(), new EventArgs());
            gSMclient.SendPollSignalQuality();
        }

        private void GSMClient_OnFindModem()
        {
            OnModemFind?.Invoke(new object(), new EventArgs());

            gSMclient.InitializeClient();
        }


        private void GSMClient_OnClientCheck()
        {
        }

        private void GSMClient_OnSomeErrorInitClient(object sender, StringEventArgs e)
        {
        }

        private void GSMClient_OnErrorClose(object sender, ByteEventArgs e)
        {
        }

        private void RestartThreadForSend()
        {
            send.Set();
        }

        private void RestartThreadForInitialization()
        {
            init.Set();
        }

        private void RestartThreadForChecked()
        {
            check.Set();
        }

        #endregion


        #region GSM Click

        public override void SendCommand(IListGSMConnection gSMConnections, CommandType commandType, bool GPSL1L2, bool GLONASSL1L2, bool GalileoAndBeidouL1L2, int FrequencyPC)
        {
            CreateMessage(commandType, GPSL1L2, GLONASSL1L2, GalileoAndBeidouL1L2, FrequencyPC);

            socketsForBuffer = gSMConnections;

            go.Set();
        }


        private void AddBufferToQuery()
        {
            foreach (var socket in socketsForBuffer)
                socetsForQuery.Add(socket);

            UpdateQuery();

            socketsForBuffer.Clear();
        }

        private void UpdateQuery()
        {
            //socetsForQuery.gSMConnections = socetsForQuery.Where(socket => socket.IsIgnore == false).ToList();
            for (int i = socetsForQuery.Count - 1; i >= 0; i--)
                if (socetsForQuery[i].IsIgnore)
                    socetsForQuery.RemoveAt(i);
        }

        private void StartSendThread()
        {
            while (true)
            {
                if (socetsForQuery.Count <= 0)
                    go.WaitOne();

                AddBufferToQuery();

                OnStartOperation.Invoke(new object(), new EventArgs());

                try
                {
                    if (socetsForQuery.Count > 0)
                    {
                        isWeCanReadCodegram = false;

                        InitializationPartOfThread();

                        SendPartOfThread();

                        ReadPartOfThread();

                        ReadALLPartOfThread();

                        UpdateQuery();
                    }


                    isWeCanReadCodegram = true;
                }
                catch (Exception ex)
                {
                    OnEndOperation?.Invoke(new object(), new EventArgs());
                    isWeCanReadCodegram = true;
                }


                OnEndOperation?.Invoke(new object(), new EventArgs());
            }
        }


        private void InitializationPartOfThread()
        {
            for (var i = 0; i < socetsForQuery.Count; i++)
                if (socetsForQuery[i].IsReadyForInitialization(permittedAmountOfReinitialization))
                {
                    GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);

                    GSMHandlerInitialization(i);
                    socetsForQuery[i].AmountOfReInitialization += 1;
                }
                else if (socetsForQuery[i].IsCanNotInitialization(permittedAmountOfReinitialization))
                {
                    socetsForQuery[i].IsIgnore = true;
                }

            WaitInitializations();
        }

        private void SendPartOfThread()
        {
            var isAtLeastOneMessageSended = false;
            for (var i = 0; i < socetsForQuery.Count; i++)
                if (socetsForQuery[i].IsReadyForSend())
                {
                    if (socetsForQuery[i].IsFirstMessageSent == false && isAtLeastOneMessageSended == false)
                        Thread.Sleep(waitingTimeAfterConnectingBeforeSend);
                    socetsForQuery[i].IsFirstMessageSent = true;

                    GSMHandlerSend(i);
                    isAtLeastOneMessageSended = true;
                }

            if (isAtLeastOneMessageSended)
            {
                WaitAllMessages(waitingTimeForCheckingSocket);

                for (var i = 0; i < socetsForQuery.Count; i++)
                    if (socetsForQuery[i].IsReadyForChecked())
                    {
                        var status = GSMHandlerChecked(socetsForQuery[i].SocketNumber);
                        socetsForQuery[i].AmountOfCheckedWithBadResult++;
                        if (status)
                        {
                            socetsForQuery[i].IsCheckedWithBadResult = false;
                            GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);
                            socetsForQuery[i].IsIgnore = true;
                            i--;
                        }
                        else if (socetsForQuery[i].AmountOfCheckedWithBadResult >= amountOfChecked)
                        {
                            GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);
                            socetsForQuery[i].IsIgnore = true;
                            i--;
                        }
                    }

                WaitAllMessages(waitingTimeForResendIfCheckingGood);

                for (var i = 0; i < socetsForQuery.Count; i++)
                    if (socetsForQuery[i].IsReadyForChecked())
                    {
                        socetsForQuery[i].AmountOfNotReceivedMessages++;

                        if (socetsForQuery[i].AmountOfNotReceivedMessages >= amountOfResend)
                        {
                            GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);
                            socetsForQuery[i].IsIgnore = true;
                            i--;
                        }
                    }
            }
        }


        private void ReadPartOfThread()
        {
            for (var i = 0; i < socetsForQuery.Count; i++)
                if (socetsForQuery[i].IsReadyForRead())
                {
                    GSMHandlerRead(socetsForQuery[i].SocketNumber);
                    socetsForQuery[i].IsIgnore = true;
                }
        }

        private void ReadALLPartOfThread()
        {
            for (var i = 0; i < socetsForQuery.Count; i++)
                if (socetsForQuery[i].IsReadyForReadAll())
                    GSMHandlerALLRead(socetsForQuery[i].SocketNumber);
        }

        #endregion


        #region Help functions

        private void WaitAllMessages(double waitTimeIfNotAllStationReady)
        {
            isWeCanReadCodegram = true;

            for (var i = 0; i < waitTimeIfNotAllStationReady; i += 200)
            {
                if (socetsForQuery.IsAllSocketGetMessageOrError())
                    break;

                Thread.Sleep(200);
            }

            isWeCanReadCodegram = false;
        }

        private void WaitInitializations()
        {
            isWeCanReadCodegram = true;
            for (var i = 0; i < waitingTimeForInitialization; i += 200)
            {
                if (socetsForQuery.IsSelectedStationsEndInitializationPart())
                    break;

                Thread.Sleep(200);
            }

            isWeCanReadCodegram = false;
        }

        #endregion


        #region Send Handlers

        private readonly TimerCallback tm = WaitTimerHandler;
        private Timer timer;

        private static void WaitTimerHandler(object obj)
        {
            if (obj != null)
                (obj as EventWaitHandle).Set();
        }


        private void GSMHandlerInitialization(int i)
        {
            timer = new Timer(tm, init, 4000, 4000);

            gSMclient.InitializeSocket(socetsForQuery[i].SocketNumber, socetsForQuery[i].IPAddress,
                socetsForQuery[i].Port);

            init.WaitOne();

            timer.Dispose();
            Thread.Sleep(300);
        }


        private void GSMHandlerSend(int i)
        {
            timer = new Timer(tm, send, 3500, 3500);

            Send(socetsForQuery[i].SocketNumber, gSMclient, socetsForQuery[i].IPAddress);
            send.WaitOne();

            timer.Dispose();
            Thread.Sleep(700);
        }


        private void GSMHandlerALLRead(byte i)
        {
            timer = new Timer(tm, readall, 3000, 3000);

            gSMclient.ReadALLCodegramm(i);
            readall.WaitOne();

            timer.Dispose();
            Thread.Sleep(300);
        }


        private bool GSMHandlerChecked(byte i)
        {
            timer = new Timer(tm, check, 4000, 4000);

            gSMclient.CheckSocketStatus(i);

            check.WaitOne();

            timer.Dispose();

            return socetsForQuery.GetBySocketNumber(i).IsCheckedWithBadResult;
        }

        private void GSMHandlerCloseSocket(byte i)
        {
            timer = new Timer(tm, close, 4000, 4000);

            gSMclient.SendCloseSocket(i);
            close.WaitOne();

            timer.Dispose();
            Thread.Sleep(300);
        }

        private void GSMHandlerRead(byte i)
        {
            gSMclient.ReadCodegramm(i);
        }

        #endregion


        #region Quaery action

        private void SomeMessageNotSended(int i)
        {
            lock (socetsForQuery)
            {
                socetsForQuery.GetBySocketNumber(i).IsSomeMessageNotSent = true;

                socetsForQuery.GetBySocketNumber(i).IsCheckedWithBadResult = true;
            }
        }

        private void CloseSocket(int i)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);
                if (!(station is null))
                {
                    station.IsInitialized = false;
                    station.IsFirstMessageSent = false;
                }
            }
        }

        private void CodegrammReceived(byte i)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);

                if (station != null)
                    station.IsIgnore = true;
            }
        }


        private void ConnectionError(int i)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);
                if (!(station is null))
                {
                    station.IsInitialized = false;
                    station.IsFirstMessageSent = false;
                    station.AmountOfCheckedWithBadResult = 0;
                    station.IsSomeErrorOccurred = true;
                    station.IsIgnore = true;
                }
            }
        }

        private void ConnectedToServer(int i)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);
                if (!(station is null))
                {
                    station.AmountOfReInitialization = 0;
                    station.IsInitialized = true;
                }
            }
        }

        #endregion


        #region Cycle poll

        private TimerCallback tmCycle;
        private Timer timerCyclePoll;

        public EventHandler OnCyclePollStarted { get; set; }
        public EventHandler OnyclePollEnded { get; set; }

        public void StartCyclePoll(StationsList stationsList, int period)
        {
            OnCyclePollStarted.Invoke(new object(), new EventArgs());

            socketsForBuffer.Clear();

            CreateMessage(CommandType.POLL, false, false, false, 0);

            tmCycle = CyclePollTimerFunctionGSM;
            timerCyclePoll = new Timer(tmCycle, stationsList, 0, period * 60000);
        }

        private void CyclePollTimerFunctionGSM(object state)
        {
            socketsForBuffer.CopyFromStationsList((StationsList)state, true);

            foreach (var station in socketsForBuffer)
            {
                station.ClearAllCountersGSMConnection();
                station.IsIgnore = false;
                station.IsSomeMessagesReaded = false;
            }

            go.Set();
        }

        public void StopCyclePoll()
        {
            timerCyclePoll?.Dispose();
        }

        #endregion


        #region Reset modem

        private Thread ResetGSM;

        public EventHandler OnResetGSMFinished { get; set; }


        public void ResetAllGSMSockets(StationsList stationsList)
        {
            sendThread?.Abort();


            socketsForBuffer.CopyFromStationsList(stationsList, true);

            ResetGSM = new Thread(ResetGSMThread);
            ResetGSM.Start();
        }

        private void ResetGSMThread()
        {
            AddBufferToQuery();
            timerCyclePoll?.Dispose();

            if (sendThread.IsAlive)
                sendThread.Abort();

            for (var i = 0; i < socetsForQuery.Count; i++) GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);

            Thread.Sleep(15000);
            socetsForQuery.Clear();
            sendThread = new Thread(StartSendThread);
            sendThread.Start();
            Thread.Sleep(5000);

            OnResetGSMFinished?.Invoke(new object(), new EventArgs());
        }

        #endregion


        #region Diagnostic features

        public void AllSocketDiagnostic()
        {
            gSMclient.CheckAllSocket();
        }

        public void ModemDiagnosticATV()
        {
            gSMclient.CheckModem_atv();
        }

        public void ModemDiagnosticSCFG()
        {
            gSMclient.CheckModem_scfg();
        }

        public void SignalQualityPoll()
        {
            gSMclient.SendPollSignalQuality();
        }
        #endregion


        public override void EndWork()
        {
            StopCyclePoll();
            ClosePort();
            timer?.Dispose();
            sendThread?.Abort();
            ResetGSM?.Abort();
            gSMclient?.EndThread();
            ready?.Close();
            read?.Close();
            readall?.Close();
            close?.Close();
            go?.Close();
            init?.Close();
            check?.Close();
            send?.Close();
        }

        public void ClosePort()
        {
            gSMclient?.ClosePort();
        }
    }
}