﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Client_New;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.ClassesForStation.Stations.List;
using Optima_3_2.Connection;
using Optima_3_2.Connection.Interfaces;
using Optima_3_2.Enums;

namespace Optima_3_2
{
    public class RadioConnection : AbstractConnection, ICyclePoll
    {
        private readonly RadioClient radioClient;
        private string radioComPort;


        #region Radio Click

        public override void SendCommand(IListGSMConnection listGSMConnection, CommandType commandType, bool GPSL1L2,
            bool GLONASSL1L2, bool GalileoAndBeidouL1L2, int FreqencyPC)
        {
            CreateMessage(commandType, GPSL1L2, GLONASSL1L2, GalileoAndBeidouL1L2, FreqencyPC);

            var SendTask = Task.Factory.StartNew(() =>
            {
                for (var i = 0; i < listGSMConnection.Count; i++)
                    Send(((EHS6)listGSMConnection[i]).StationModel.Number - 1, radioClient,
                        ((EHS6)listGSMConnection[i]).StationModel.IPAddress);
            });
        }

        #endregion

        #region Properties

        public EventHandler<StringEventArgs> OnCodegrammRecieved => radioClient.OnCodegramRecieved;

        public string LocalIP
        {
            get => radioClient.LocalIP;
            set => radioClient.LocalIP = value;
        }

        public override string ComPort
        {
            get => radioComPort;
            set
            {
                if (radioComPort == value)
                {
                    radioComPort = value;
                    radioClient.OpenPort(radioComPort, 9600, Parity.None, 8, StopBits.One,
                        ClientType.GSM);

                    OnCOMPortChanged.Invoke(new object(), new StringEventArgs(value));
                }
            }
        }

        #endregion


        #region Methods

        public RadioConnection(string radioComPort)
        {
            ComPort = radioComPort;


            OnCOMPortChanged += PropertiesPropGrid_OnComPortChanged;

            radioClient = new RadioClient();
            radioClient.OpenPort(radioComPort, 9600, Parity.None, 8, StopBits.One,
                ClientType.Radio);
        }


        private void PropertiesPropGrid_OnComPortChanged(object sender, StringEventArgs e)
        {
            radioClient.OpenPort(radioComPort, 9600, Parity.None, 8, StopBits.One,
                ClientType.Radio);
        }

        private void Radio_Checked(object sender, RoutedEventArgs e)
        {
        }

        #endregion


        #region Cycle poll

        private TimerCallback tmCycle;
        private Timer timerCyclePoll;

        public EventHandler OnCyclePollStarted { get; set; }
        public EventHandler OnyclePollEnded { get; set; }

        public void StartCyclePoll(StationsList stationsList, int period)
        {
            OnCyclePollStarted.Invoke(new object(), new EventArgs());


            CreateMessage(CommandType.POLL, false, false, false, 0);

            tmCycle = CyclePollTimerFunctionRadio;
            timerCyclePoll = new Timer(tmCycle, stationsList, 0, period * 60000);
        }

        private void CyclePollTimerFunctionRadio(object state)
        {
            var SendTask = Task.Factory.StartNew(() =>
            {
                var stationList = (StationsList)state;

                for (var i = 0; i < stationList.Count; i++)
                {
                    Send(stationList[i].StationModel.Number - 1, radioClient, stationList[i].StationModel.IPAddress);
                    Thread.Sleep(200);
                }
            });
            OnyclePollEnded.Invoke(new object(), new EventArgs());
        }


        public void StopCyclePoll()
        {
            timerCyclePoll.Dispose();
        }

        public override void EndWork()
        {
            StopCyclePoll();
        }

        #endregion
    }
}