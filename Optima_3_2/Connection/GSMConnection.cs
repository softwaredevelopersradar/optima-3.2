﻿using Client_New;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.ClassesForStation.Stations.List;
using Optima_3_2.Connection.Interfaces;
using PropertyGridStation.Model;
using ProtocolOptima;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using CommandType = Optima_3_2.Enums.CommandType;

namespace Optima_3_2.Connection
{
    public class GSMConnection: AbstractConnection, ICyclePoll, IResetConnection
    {
        public EventHandler OnStartOperation;
        public EventHandler OnEndOperation;
        public EventHandler<ListGSMConnection> UpdateStationList;
        public EventHandler<IntegerEventArgs> OnSomeMessageNotSended;
        public EventHandler<IntegerEventArgs> OnCloseSocket;
        public EventHandler<IntegerEventArgs> OnSomeErrorInitSocket;
        public EventHandler<IntegerEventArgs> OnCodegrammReceived;
        public EventHandler<IntegerEventArgs> OnCodegramSended;
        public EventHandler<IntegerEventArgs> OnConfirmedSendCodegram;
        public EventHandler<IntegerEventArgs> OnConnectionError;
        public EventHandler<IntegerEventArgs> OnConnectedToServer;
        public EventHandler<IntegerEventArgs> OnSomeSocketInitialized;
        public EventHandler OnClientRegistered;
        public EventHandler OnModemFind;
        public EventHandler OnSocketChecked;

        public EventHandler<StringEventArgs> OnRead;
        public EventHandler<BytesEventArgs> OnWriteCodegram;
        public EventHandler<StringEventArgs> OnWriteCommand;



        private static readonly EventWaitHandle ready = new AutoResetEvent(false);
        private static readonly EventWaitHandle go = new AutoResetEvent(false);
        private static readonly EventWaitHandle send = new AutoResetEvent(false);
        private static readonly EventWaitHandle init = new AutoResetEvent(false);
        private static readonly EventWaitHandle check = new AutoResetEvent(false);
        private static readonly EventWaitHandle close = new AutoResetEvent(false);
        private static readonly EventWaitHandle read = new AutoResetEvent(false);
        private static readonly EventWaitHandle readall = new AutoResetEvent(false);


        private readonly GSMClient gSMclient = new GSMClient();
        private readonly ListGSMConnection socetsForQuery = new ListGSMConnection();
        private ListGSMConnection socketsForBuffer = new ListGSMConnection();

        private bool isWeCanReadCodegram = true;
        private Thread sendThread;

        private string comPortGSM = "COM1";
        private int amountOfChecked = 2;
        private int amountOfResend = 2;
        private int permittedAmountOfReinitialization = 1;

        private int waitingTimeAfterConnectingBeforeSend = 15000;
        private int waitingTimeForCheckingSocket = 28000;
        private int waitingTimeForInitialization = 38000;
        private int waitingTimeForResendIfCheckingGood = 12000;


        #region Propeties
        public override string ComPort
        {
            get { return comPortGSM; }
            set {
                if (comPortGSM != value)
                {
                    comPortGSM = value;
                    gSMclient.OpenPort(comPortGSM, 9600, Parity.None, 8, StopBits.One,
                        ClientType.GSM);
                    gSMclient.SendStartCommand();

                    OnCOMPortChanged.Invoke(new Object(), new StringEventArgs(comPortGSM));
                }
            }
        }


        public int AmountOfChecked
        {
            get { return amountOfChecked; }
            set
            {
                if (amountOfChecked == value)
                    return;
                amountOfChecked = value;
            }
        }


        public int AmountOfResend
        {
            get { return amountOfResend; }
            set
            {
                if (amountOfResend == value)
                    return;
                amountOfResend = value;
            }
        }

        public int PermittedAmountOfReinitialization
        {
            get { return permittedAmountOfReinitialization; }
            set
            {
                if (permittedAmountOfReinitialization == value)
                    return;
                permittedAmountOfReinitialization = value;
            }
        }

        public int WaitingTimeAfterConnectingBeforeSend
        {
            get { return waitingTimeAfterConnectingBeforeSend; }
            set
            {
                if (waitingTimeAfterConnectingBeforeSend == value)
                    return;
                waitingTimeAfterConnectingBeforeSend = value;
            }
        }

        public int WaitingTimeForCheckingSocket
        {
            get { return waitingTimeForCheckingSocket; }
            set
            {
                if (waitingTimeForCheckingSocket == value)
                    return;
                waitingTimeForCheckingSocket = value;
            }
        }

        public int WaitingTimeForInitialization
        {
            get { return waitingTimeForInitialization; }
            set
            {
                if (waitingTimeForInitialization == value)
                    return;
                waitingTimeForInitialization = value;
            }
        }

        public int WaitingTimeForResendIfCheckingGood
        {
            get { return waitingTimeForResendIfCheckingGood; }
            set
            {
                if (waitingTimeForResendIfCheckingGood == value)
                    return;
                waitingTimeForResendIfCheckingGood = value;
            }
        }
        #endregion


        #region Initialize Features

        public GSMConnection(string comPortGSM, string iPAdressControlStation)
        {
            gSMclient = new GSMClient();

            gSMclient.LocalIP = iPAdressControlStation;

            OnCOMPortChanged += GSM_OnGSMCOMPortChanged;

            gSMclient.OnSocketInitialized += GSMClient_OnSocketInitialized;
            gSMclient.OnSocketConnected += GSMClient_ConnectedToServer;
            gSMclient.OnClientRegistered += GSMClient_ClientRegistered;
            gSMclient.OnCodegramRecieved += GSMClient_CodegrammReceived;
            gSMclient.OnCodegramSended += GSMClient_OnCodegramSended;
            gSMclient.OnErrorOcured += GSMClient_ConnectionError;
            gSMclient.OnFindModem += GSMClient_OnFindModem;
            gSMclient.OnConfirmedSendCodegram += GSMClient_OnConfirmedSendCodegram;
            gSMclient.OnSomeErrorInitSocket += GSMClient_OnSomeErrorInitSocket;
            gSMclient.OnSomeErrorWrite += GSMClient_OnSomeErrorWrite;
            gSMclient.OnSomeMessageNotSended += GSMClient_OnSomeMessageNotSended;
            gSMclient.OnSomeMessageRecived += GSMClient_OnSomeMessageRecived;
            gSMclient.OnSocketRecivedBufferClear += GSMClient_OnSocketRecivedBufferClear;
            gSMclient.OnCodegramDecode += GSMClient_OnCodegramDecoded;
            gSMclient.OnSocketChecked += GSMClient_OnSocketChecked;
            gSMclient.OnSocketClosed += GSMclient_OnSocketClosed;
            gSMclient.OnSocketClear += GSMClient_OnSocketClear;
            gSMclient.OnErrorClear += GSMClient_OnErrorClear;

            gSMclient.OnClientCheck += GSMClient_OnClientCheck;
            gSMclient.OnErrorOcured += GSMClient_OnErrorClose;
            gSMclient.OnErrorClose += GSMClient_OnErrorClose;
            gSMclient.OnReadyToSendCodegram += GSMClient_OnErrorClose;
            gSMclient.OnSomeErrorInitClient += GSMClient_OnSomeErrorInitClient;
            gSMclient.OnSomeErrorRead += GSMClient_OnSomeErrorInitClient;

            gSMclient.OnWriteString += OnWriteComand;
            gSMclient.OnWriteByte += OnWriteCodeg;
            gSMclient.OnReadString += OnReadComand;
            gSMclient.AmountOfTrySendCommand = 5;

            ComPort = comPortGSM;
            gSMclient.OpenPort(comPortGSM, 9600, Parity.None, 8, StopBits.One, ClientType.GSM);
            gSMclient.SendStartCommand();



            sendThread = new Thread(StartSendThread);
            sendThread.Start();
        }

        private void GSM_OnGSMCOMPortChanged(object sender, EventArgs e)
        {
            gSMclient.OpenPort(ComPort, 9600, Parity.None, 8, StopBits.One,
             ClientType.GSM);
            gSMclient.SendStartCommand();
        }

        private void OnReadComand(object sender, StringEventArgs e)
        {
            OnRead.Invoke(sender, e);
        }

        private void OnWriteCodeg(object sender, BytesEventArgs e)
        {
            OnWriteCodegram.Invoke(sender, e);
        }

        private void OnWriteComand(object sender, StringEventArgs e)
        {
            if(OnWriteCommand != null)
                OnWriteCommand.Invoke(sender, e);
        }

        #endregion


        #region GSM Events handlers

        private List<byte> socketsWithMessages = new List<byte>();

        private void GSMClient_OnSomeMessageRecived(object sender, ByteEventArgs e)
        {

            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(e.Data);

                if (station != null)
                {

                    if (isWeCanReadCodegram)
                    {
                        isWeCanReadCodegram = false;
                        station.IsIgnore = true;
                        GSMHandlerRead(e.Data);
                    }
                    else station.IsSomeMessageReceived = true;
                }
                else
                {
                    if (isWeCanReadCodegram)
                        GSMHandlerRead(e.Data);
                    else socketsWithMessages.Add(e.Data);
                }
            }
        }


        private void GSMClient_OnCodegramDecoded(object sender, ByteEventArgs e)
        {
            lock (socetsForQuery)
            {
                isWeCanReadCodegram = true;
                var station = socetsForQuery.GetBySocketNumber(e.Data);

                if (station != null)
                {
                    station.IsSomeMessagesReaded = true;
                }
                else
                {
                    isWeCanReadCodegram = false;
                    GSMHandlerRead(socketsWithMessages[0]);
                    socketsWithMessages.RemoveAt(0);
                    return;
                }


                for (int i = 0; i < socetsForQuery.Count; i++)
                {
                    if (socetsForQuery[i].IsSomeMessageReceived)
                    {
                        isWeCanReadCodegram = false;
                        GSMHandlerRead(socetsForQuery[i].SocketNumber);
                        socetsForQuery[i].IsIgnore = true;
                        socetsForQuery[i].IsSomeMessageReceived = false;
                        break;
                    }
                }
            }
        }

        private void GSMClient_OnSocketRecivedBufferClear(object sender, ByteEventArgs e)
        {
            readall.Set();
        }


        private void GSMClient_OnSomeMessageNotSended(object sender, ByteEventArgs e)
        {
            SomeMessageNotSended(e.Data);

            OnSomeMessageNotSended.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            ready.Set();
        }


        private void GSMclient_OnSocketClosed(object sender, ByteEventArgs e)
        {
            CloseSocket(e.Data);

            OnCloseSocket.Invoke(sender, new IntegerEventArgs(e.Data + 1));
        }

        private void GSMClient_OnSocketClear(object sender, ByteEventArgs e)
        {
            close.Set();
        }

        private void GSMClient_OnErrorClear(object sender, ByteEventArgs e)
        {
            close.Set();
        }

        private void GSMClient_OnSomeErrorWrite(object sender, StringEventArgs e)
        {

        }

        private void GSMClient_OnSomeErrorInitSocket(object sender, ByteEventArgs e)
        {
            OnSomeErrorInitSocket.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            RestartThreadForInitialization();
        }

        private void GSMClient_CodegrammReceived(object sender, ByteEventArgs e)
        {
            var codegram = (StationCodegram)sender;
            CodegrammReceived(e.Data);

            OnCodegrammReceived.Invoke(codegram, new IntegerEventArgs(e.Data + 1));
            read.Set();
        }

        private void GSMClient_OnSocketChecked(object sender, ByteEventArgs e)
        {
            OnSocketChecked.Invoke(sender, new EventArgs());

            RestartThreadForChecked();
        }

        private void GSMClient_OnCodegramSended(object sender, ByteEventArgs e)
        {
            OnCodegramSended.Invoke(sender, new IntegerEventArgs(e.Data + 1));
        }

        private void GSMClient_OnConfirmedSendCodegram(object sender, ByteEventArgs e)
        {
            OnConfirmedSendCodegram.Invoke(sender, new IntegerEventArgs(e.Data + 1));

            RestartThreadForSend();
        }

        private void GSMClient_ConnectionError(object sender, ByteEventArgs e)
        {
            OnConnectionError.Invoke(send, new IntegerEventArgs(e.Data + 1));

            ConnectionError(e.Data);

            ready.Set();
        }

        private void GSMClient_ConnectedToServer(object sender, ByteEventArgs e)
        {
            ConnectedToServer(e.Data);

            OnConnectedToServer(sender, new IntegerEventArgs(e.Data + 1));

            ready.Set();
        }

        private void GSMClient_OnSocketInitialized(object sender, ByteEventArgs e)
        {
            RestartThreadForInitialization();

            OnSomeSocketInitialized.Invoke(sender, new IntegerEventArgs(e.Data + 1));
        }

        private void GSMClient_ClientRegistered()
        {
            OnClientRegistered.Invoke(new object(), new EventArgs());
        }

        private void GSMClient_OnFindModem()
        {

            OnModemFind.Invoke(new object(), new EventArgs());

            gSMclient.InitializeClient();
        }


        private void GSMClient_OnClientCheck()
        {

        }

        private void GSMClient_OnSomeErrorInitClient(object sender, StringEventArgs e)
        {

        }

        private void GSMClient_OnErrorClose(object sender, ByteEventArgs e)
        {

        }

        private void RestartThreadForSend()
        {
            send.Set();
        }

        private void RestartThreadForInitialization()
        {
            init.Set();
        }

        private void RestartThreadForChecked()
        {
            check.Set();
        }

        #endregion


        #region GSM Click

        public override void SendCommand(ListGSMConnection gSMConnections, CommandType commandType, bool GPSL1L2, bool GLONASSL1L2, bool GalileoAndBeidouL1L2, int FrequencyPC)
        {
            CreateMessage(commandType, GPSL1L2, GLONASSL1L2, GalileoAndBeidouL1L2, FrequencyPC);

            socketsForBuffer = gSMConnections;

            go.Set();
        }


        private void AddBufferToQuery()
        {
            //UpdateStationList.Invoke(new object(), socetsForQuery);

            foreach (var socket in socketsForBuffer)
            {
                socetsForQuery.Add(socket);
            }

            UpdateQuery();

            socketsForBuffer.Clear();
        }
        
        private void UpdateQuery()
        {
            foreach (var socket in socetsForQuery)
            {
                if (socket.IsIgnore)
                    socetsForQuery.Remove(socket);
            }
        }

        private void StartSendThread()
        {
            while (true)
            {
                if (socetsForQuery.Count <= 0)
                    go.WaitOne();

                AddBufferToQuery();

                OnStartOperation.Invoke(new object(), new EventArgs());

                try
                {

                    if (socetsForQuery.Count > 0)
                    {
                        isWeCanReadCodegram = false;

                        InitializationPartOfThread();

                        SendPartOfThread();

                        ReadPartOfThread();

                        ReadALLPartOfThread();

                        UpdateQuery();
                    }


                    isWeCanReadCodegram = true;
                }
                catch (Exception ex)
                {
                    OnEndOperation.Invoke(new object(), new EventArgs());
                    isWeCanReadCodegram = true;
                }


                OnEndOperation.Invoke(new object(), new EventArgs());
            }
        }



        private void InitializationPartOfThread()
        {
            for (var i = 0; i < socetsForQuery.Count; i++)
            {
                if (socetsForQuery[i].IsReadyForInitialization(permittedAmountOfReinitialization))
                {
                    GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);

                    GSMHandlerInitialization(i);
                    socetsForQuery[i].AmountOfReInitialization += 1;
                }
                else if (socetsForQuery[i].IsCanNotInitialization(permittedAmountOfReinitialization))
                    socetsForQuery[i].IsIgnore = true;

            }

           // UpdateStationList.Invoke(new object(), socetsForQuery);

            WaitInitializations();

        }

        private void SendPartOfThread()
        {
            var isAtLeastOneMessageSended = false;
            for (var i = 0; i < socetsForQuery.Count; i++)
            {
                if (socetsForQuery[i].IsReadyForSend())
                {
                    if (socetsForQuery[i].IsFirstMessageSent == false && isAtLeastOneMessageSended == false)
                        Thread.Sleep(waitingTimeAfterConnectingBeforeSend);
                    socetsForQuery[i].IsFirstMessageSent = true;

                    GSMHandlerSend(i);
                    isAtLeastOneMessageSended = true;
                }

            }

            if (isAtLeastOneMessageSended)
            {
                WaitAllMessages(waitingTimeForCheckingSocket);

                for (var i = 0; i < socetsForQuery.Count; i++)
                    if (socetsForQuery[i].IsReadyForChecked())
                    {
                        var status = GSMHandlerChecked(socetsForQuery[i].SocketNumber);
                        if (status == false)
                        {
                            socetsForQuery[i].AmountOfCheckedWithBadResult++;
                            socetsForQuery[i].IsCheckedWithBadResult = false;

                            if (socetsForQuery[i].AmountOfCheckedWithBadResult >= amountOfChecked)
                            {
                                GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);
                                socetsForQuery[i].IsIgnore = true;
                                i--;
                            }
                        }
                    }

                WaitAllMessages(waitingTimeForResendIfCheckingGood);

                for (var i = 0; i < socetsForQuery.Count; i++)
                    if (socetsForQuery[i].IsReadyForChecked())
                    {
                        socetsForQuery[i].AmountOfNotReceivedMessages++;

                        if (socetsForQuery[i].AmountOfNotReceivedMessages >= amountOfResend)
                        {
                            GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);
                            socetsForQuery[i].IsIgnore = true;
                            i--;
                        }
                    }
            }

            //UpdateStationList.Invoke(new object(), socetsForQuery);
        }


        private void ReadPartOfThread()
        {
            for (var i = 0; i < socetsForQuery.Count; i++)
                if (socetsForQuery[i].IsReadyForRead())
                {
                    GSMHandlerRead(socetsForQuery[i].SocketNumber);
                    socetsForQuery[i].IsIgnore = true;
                }
        }

        private void ReadALLPartOfThread()
        {
            for (var i = 0; i < socetsForQuery.Count; i++)
                if (socetsForQuery[i].IsReadyForReadAll())
                    GSMHandlerALLRead(socetsForQuery[i].SocketNumber);
            //UpdateStationList.Invoke(new object(), socetsForQuery);
        }

        #endregion


        #region Help functions

        private void WaitAllMessages(double waitTimeIfNotAllStationReady)
        {
            isWeCanReadCodegram = true;

            for (int i = 0; i < waitTimeIfNotAllStationReady; i += 200)
            {
                if (socetsForQuery.IsAllSocketGetMessageOrError())
                    break;

                Thread.Sleep(200);
            }

            isWeCanReadCodegram = false;
        }

        private void WaitInitializations()
        {
            isWeCanReadCodegram = true;
            for (int i = 0; i < waitingTimeForInitialization; i += 200)
            {
                if (socetsForQuery.IsSelectedStationsEndInitializationPart())
                    break;

                Thread.Sleep(200);
            }
            isWeCanReadCodegram = false;
        }

        #endregion


        #region Send Handlers


        private readonly TimerCallback tm = WaitTimerHandler;
        Timer timer;
        private static void WaitTimerHandler(object obj)
        {
            if (obj != null)
                (obj as EventWaitHandle).Set();
        }



        private void GSMHandlerInitialization(int i)
        {
            timer = new Timer(tm, init, 4000, 200);

            gSMclient.InitializeSocket(socetsForQuery[i].SocketNumber, ((EHS6)socetsForQuery[i]).StationModel.IPAddress,
                ((EHS6Model)((EHS6)socetsForQuery[i]).StationModel).Port);

            init.WaitOne();

            timer.Dispose();
        }


        private void GSMHandlerSend(int i)
        {
            timer = new Timer(tm, send, 3500, 200);

            Send(socetsForQuery[i].SocketNumber, gSMclient, ((EHS6)socetsForQuery[i]).StationModel.IPAddress);
            send.WaitOne();

            timer.Dispose();
            Thread.Sleep(700);
        }


        private void GSMHandlerALLRead(byte i)
        {
            timer = new Timer(tm, readall, 3000, 200);

            gSMclient.ReadALLCodegramm(i);
            readall.WaitOne();

            timer.Dispose();
            Thread.Sleep(300);
        }


        private bool GSMHandlerChecked(byte i)
        {
            timer = new Timer(tm, check, 4000, 200);

            gSMclient.CheckSocketStatus(i);

            check.WaitOne();
            if (socetsForQuery.GetBySocketNumber(i).IsCheckedWithBadResult)
                return false;
            return true;

            timer.Dispose();
        }

        private void GSMHandlerCloseSocket(byte i)
        {
            timer = new Timer(tm, close, 4000, 200);

            gSMclient.SendCloseSocket(i);
            close.WaitOne();

            timer.Dispose();
        }

        private void GSMHandlerRead(byte i)
        {
            gSMclient.ReadCodegramm(i);
        }

        #endregion


        #region Quaery action

        private void SomeMessageNotSended(int i)
        {
            lock (socetsForQuery)
            {
                socetsForQuery.GetBySocketNumber(i).IsSomeMessageNotSent = true;

                socetsForQuery.GetBySocketNumber(i).IsCheckedWithBadResult = true;
            }
        }

        private void CloseSocket(int i)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);
                if (!(station is null))
                {
                    station.IsInitialized = false;
                    station.IsFirstMessageSent = false;
                }
            }
        }

        private void CodegrammReceived(byte i)
        {          
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);

                if (station != null)
                    station.IsIgnore = true;
            }

        }

       

        private void ConnectionError(int i)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);
                if (!(station is null))
                {
                    station.IsInitialized = false;
                    station.IsFirstMessageSent = false;
                    station.AmountOfCheckedWithBadResult = 0;
                    station.IsSomeErrorOccurred = true;
                    station.IsIgnore = true;
                }
            }
        }

        private void ConnectedToServer(int i)
        {
            lock (socetsForQuery)
            {
                var station = socetsForQuery.GetBySocketNumber(i);
                if (!(station is null))
                {
                    station.AmountOfReInitialization = 0;
                    station.IsInitialized = true;
                }
            }     
        }

        #endregion


        #region Cycle poll

        private TimerCallback tmCycle;
        private Timer timerCyclePoll;

        public EventHandler OnCyclePollStarted { get; set; }
        public EventHandler OnyclePollEnded { get; set; }

        public void StartCyclePoll(StationsList stationsList, int period)
        {
            OnCyclePollStarted.Invoke(new object(), new EventArgs());

            socketsForBuffer.Clear();

            CreateMessage(Enums.CommandType.POLL, false, false, false, 0);

            tmCycle = CyclePollTimerFunctionGSM;
            timerCyclePoll = new Timer(tmCycle, stationsList, 0, period * 60000);
        }

        private void CyclePollTimerFunctionGSM(object state)
        {
            socketsForBuffer.CopyFromStationsList((StationsList)state, true);

            foreach (var station in socketsForBuffer)
            {
                station.ClearAllCountersGSMConnection();
                station.IsIgnore = false;
                station.IsSomeMessagesReaded = false;
            }

            go.Set();
        }

        public void StopCyclePoll()
        {
            timerCyclePoll?.Dispose();
        }

        #endregion


        #region Reset modem

        private Thread ResetGSM;

        public EventHandler OnResetGSMFinished { get; set; }


        public void ResetAllGSMSockets(StationsList stationsList)
        {
            sendThread?.Abort();


            socketsForBuffer.CopyFromStationsList(stationsList, true);

            ResetGSM = new Thread(ResetGSMThread);
            ResetGSM.Start();
        }

        private void ResetGSMThread()
        {
            AddBufferToQuery();
            timerCyclePoll?.Dispose();

            if (sendThread.IsAlive)
                sendThread.Abort();

            for (var i = 0; i < socetsForQuery.Count; i++)
            {
                GSMHandlerCloseSocket(socetsForQuery[i].SocketNumber);
            }

            Thread.Sleep(15000);
            socetsForQuery.Clear();
            sendThread = new Thread(StartSendThread);
            sendThread.Start();
            Thread.Sleep(5000);

            OnResetGSMFinished.Invoke(new object(), new EventArgs());
        }

        #endregion

        public override void EndWork()
        {
            StopCyclePoll();

            timer?.Dispose();
            sendThread?.Abort();
            ResetGSM?.Abort();
            gSMclient?.EndThread();
            ready?.Close();
            read?.Close();
            readall?.Close();
            close?.Close();
            go?.Close();
            init?.Close();
            check?.Close();
            send?.Close();
        }
    }
}
