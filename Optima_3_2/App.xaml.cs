﻿using System.Threading;
using System.Windows;

namespace Optima_3_2
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Mutex _mutex;

        protected override void OnStartup(StartupEventArgs e)
        {
            const string appName = "Optima_3_2";
            bool createdNew;

            _mutex = new Mutex(true, appName, out createdNew);

            if (!createdNew) Current.Shutdown();

            base.OnStartup(e);
        }
    }
}