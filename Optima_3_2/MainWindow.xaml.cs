﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using Mapsui.Projection;
using Mapsui.Styles;
using ModemControl;
using Optima_3_2.ClassesForStation.List;
using Optima_3_2.ClassesForStation.Stations.Connection;
using Optima_3_2.ClassesForStation.Stations.List;
using PropertyGridSettings.Model;
using WPFControlConnection;
using WpfMapControl;

namespace Optima_3_2
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int _amountOfStation = 10; //поставить можно любое количество станций. Влияет только на количество строк в таблице и обновление контекстного меню карты

        private readonly ControlStationsList controlStationsList;
        private readonly List<ModemModel> listModem = new List<ModemModel>();
        private readonly MainWindowViewModel mainWindowViewModel;
        private readonly StationsList stationsList;

        private MapObjectStyle _controlStationStyle;
        private MapObjectStyle _controlStationStyleWithNote;
        private MapObjectStyle _stationStyle;
        private MapObjectStyle _stationStyleWithNote;
        private MapObjectStyle _subControlStationStyle;
        private MapObjectStyle _subControlStationStyleWithNote;

        private string stationName;


        public MainWindow()
        {
            InitializeComponent();

            stationsList = new StationsList();
            controlStationsList = new ControlStationsList();
            mainWindowViewModel = new MainWindowViewModel();
            mainWindowViewModel.PropertyChanged += MainWindowViewModel_PropertyChanged;
            Table.AmountOfNeseseryRows = _amountOfStation;

            InitSettings();

            SetLanguageForLog();
            SetLanguageForTable();
            propertyGridStation.Language = propertiesPropGrid.Local.Common.Language;
            propertyGridControlStation.Language = propertiesPropGrid.Local.Common.Language;

            InitRasterMap();
            ChangeAllStationIconOfMapMenuItem();
            ChangeAllControlIconOfMapMenuItem();
            StyleInit();
            OpenMap();
            InitViewParamMap();

            LoadControlStations();
            LoadStations();

            TableToggleButton.IsChecked = true;
            DownPanelToggleButton.IsChecked = true;


            InitializeGSMConnection();
            InitializationRadio();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            CloseConnectionLogger();
            try
            {
                comARD?.SendSetAngle(0, (short)propertiesPropGrid.Local.BRD.CourseAngle, true);
                comARD?.ClosePort();
            }
            catch
            {
            }

            gSMConnection?.EndWork();

            Dispatcher.ExitAllFrames();
        }

        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {
            //radioClient_1?.SwitchEadioFrequency(false);
        }

        private void ComboBoxItem_Selected_1(object sender, RoutedEventArgs e)
        {
            // radioClient_1?.SwitchEadioFrequency(true);
        }

        private void ResetGSMToggleButton_Click(object sender, RoutedEventArgs e)
        {
            EnableGSM(false);
            EnableGSMFix(false);
            

            var listGSMConnections = new ListGSMConnection();
            listGSMConnections.CopyFromStationsList(stationsList, true);
            gSMConnection.ResetAllGSMSockets(stationsList);
        }

        private void Radio_Checked(object sender, RoutedEventArgs e)
        {
            EnableSend(true);
        }

        private void SendCyclePollButton_Checked(object sender, RoutedEventArgs e)
        {
            gSMConnection.StartCyclePoll(stationsList, propertiesPropGrid.Local.Common.CyclePool_Minutes);
        }

        private void SendCyclePollButton_Unchecked(object sender, RoutedEventArgs e)
        {
            Log.AddTextToLog(" ~~~~ End cycle poll ~~~~", commentColorBrush);
            gSMConnection.StopCyclePoll();
        }

        private void propertiesPropGrid_OnComPortGSMChanged(object sender, string e)
        {
       
        }


        #region FormatCordinate

        private void OnCoordinateFormatChanged(object sender, ViewCoord e)
        {
            stationsList.SetCoordinateViewForAllModels(e);
            controlStationsList.SetCoordinateViewForAllModels(e);
        }

        #endregion


        #region Initialize Features

        private void InitRasterMap()
        {
            if (!propertiesPropGrid.Local.OEM.Availability)
                RasterMap.VisibleZoneOptic = false;
            if (!propertiesPropGrid.Local.BRD.Availability)
                RasterMap.VisibleZoneBearing = false;
        }

        private void ChangeAllStationIconOfMapMenuItem()
        {
            for (var i = 0; i < _amountOfStation; i++)
                RasterMap.ChangeIconOfMenuItemStation(i, Environment.CurrentDirectory + "/Resources/plus.png");
        }

        private void ChangeAllControlIconOfMapMenuItem()
        {
            RasterMap.ChangeIconOfAllControlStations(Environment.CurrentDirectory + "/Resources/plus.png");
        }

        private void InitSettings()
        {
            InitializeLangugeDictionary(propertiesPropGrid.Local.Common.Language);

            ApplySettings();
        }

        private void OpenMap()
        {
            if (RasterMap.PathMap != null)
                try
                {
                    RasterMap.OpenMap();
                }
                catch (Exception)
                {
                    MessageBox.Show("Map not found");
                }
        }

        private void StyleInit()
        {
            _stationStyle = RasterMap.mapControl.LoadObjectStyle(
                Environment.CurrentDirectory + "/Resources/Station_1.png", new Offset(0, 29), 0.2, new Offset(0, -32));
            _controlStationStyle = RasterMap.mapControl.LoadObjectStyle(
                Environment.CurrentDirectory + "/Resources/Asy_1.png", new Offset(0, 29), 0.151, new Offset(0, -27));
            _stationStyleWithNote = RasterMap.mapControl.LoadObjectStyle(
                Environment.CurrentDirectory + "/Resources/Station_1.png", new Offset(0, 29), 0.2, new Offset(0, -55));
            _controlStationStyleWithNote = RasterMap.mapControl.LoadObjectStyle(
                Environment.CurrentDirectory + "/Resources/Asy_1.png", new Offset(0, 29), 0.151, new Offset(0, -50));
            _subControlStationStyle = RasterMap.mapControl.LoadObjectStyle(
                Environment.CurrentDirectory + "/Resources/Asy_1.png", new Offset(0, 29), 0.2, new Offset(0, -32));
            _subControlStationStyleWithNote = RasterMap.mapControl.LoadObjectStyle(
                Environment.CurrentDirectory + "/Resources/Asy_1.png", new Offset(0, 29), 0.2, new Offset(0, -55));
        }

        #endregion


        #region Map Events

        private void RasterMap_OnMapOpen(object sender, string e)
        {
            SaveMapPath(e);
            DrawAllStations();
            DrawAllControlStation();
        }


        private void SaveMapPath(string path)
        {
            propertiesPropGrid.Local.Map.FileMap = path;
        }

        private void RasterMap_OnOnCoordinateForSaveUpdate(object sender, Location e)
        {
            propertiesPropGrid.Local.Map.CurrentScale = RasterMap.CurrentScale;
            propertiesPropGrid.Local.Map.Latitude = e.Latitude;
            propertiesPropGrid.Local.Map.Longitude = e.Longitude;

            RasterMap.GNSSPosition = new Location(e.Longitude, e.Latitude);

            Yaml.YamlSaveLocalProperties(propertiesPropGrid.Local);
        }

        #endregion


        #region Settings

        private void propertiesPropGrid_OnDefaultButtonClick(object sender, LocalProperties e)
        {
            propertiesPropGrid.Local.Common.ComPort_GSM = "COM1";
            propertiesPropGrid.Local.Common.ComPort_Radio = "COM1";
            propertiesPropGrid.Local.Common.CoordinateView = ViewCoord.Dd;
            propertiesPropGrid.Local.Common.Language = Languages.EN;

            propertiesPropGrid.Local.Map.FileMap = "";
            propertiesPropGrid.Local.Map.FolderMapTiles = "";
            propertiesPropGrid.Local.Map.FileDTEDElevation = "";
            propertiesPropGrid.Local.Map.ImageASY = Environment.CurrentDirectory + "/Resources/Asy_2.png";
            propertiesPropGrid.Local.Map.ImageStation = Environment.CurrentDirectory + "/Resources/Station_2.png";

            propertiesPropGrid.Local.BRD.Availability = false;
            propertiesPropGrid.Local.BRD.Address = 4;
            propertiesPropGrid.Local.BRD.CourseAngle = 0;
            propertiesPropGrid.Local.BRD.PortSpeed = 1;
            propertiesPropGrid.Local.BRD.ComPort = "COM1";

            propertiesPropGrid.Local.OEM.CourseAngle = 0;
            propertiesPropGrid.Local.OEM.Availability = false;
            propertiesPropGrid.Local.OEM.IpAddressLocal = "127.0.0.1";
            propertiesPropGrid.Local.OEM.PortLocal = 80;
            propertiesPropGrid.Local.OEM.PortRemoute = 80;
            propertiesPropGrid.Local.OEM.IpAddressRemoute = "127.0.0.1";
        }

        private void propertiesPropGrid_OnCourseAngleChanged(object sender, float e)
        {
            if (mainWindowViewModel != null && mainWindowViewModel.DirectionOEM != null)
                mainWindowViewModel.DirectionOEM.CourseAngle = e;
        }

        private void propertiesPropGrid_OnCourseBRDAngleChanged(object sender, float e)
        {
            if (mainWindowViewModel != null && mainWindowViewModel.DirectionBRD != null)
                mainWindowViewModel.DirectionBRD.CourseAngle = e;
        }

        private void propertiesPropGrid_OnBRDAngleAvailability(object sender, bool e)
        {
            RasterMap.VisibleZoneBearing = e;
        }

        private void propertiesPropGrid_OnOEMAngleAvailability(object sender, bool e)
        {
            RasterMap.VisibleZoneOptic = e;
        }

        private void ApplySettings()
        {
            try
            {
                propertiesPropGrid.Local =
                    Yaml.YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "LocalProperties.yaml");
            }
            catch (Exception)
            {
                Console.WriteLine("LocalProperties.yaml not found");
            }
        }

        private void InitViewParamMap()
        {
            try
            {
                RasterMap.SetScale(propertiesPropGrid.Local.Map.CurrentScale);

                var center = new Location(propertiesPropGrid.Local.Map.Longitude,
                    propertiesPropGrid.Local.Map.Latitude);

                RasterMap.GNSSPosition = center;

                var centerPoint = Mercator.FromLonLat(propertiesPropGrid.Local.Map.Longitude,
                    propertiesPropGrid.Local.Map.Latitude);
                RasterMap.mapControl.NavigateTo(centerPoint);
            }
            catch
            {
            }
        }

        private void PropertiesPropGrid_OnApplyButtonClick(object sender, LocalProperties e)
        {
            Yaml.YamlSaveLocalProperties(e);
        }

        #endregion


        #region VisibilityPanels

        private GridLength _h;

        private void TableToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            RowTable.Height = _h;
        }

        private void TableToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            _h = RowTable.Height;
            RowTable.Height = new GridLength(0, GridUnitType.Pixel);
        }

        private void DownPanelToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            RowDownPanel.Height = new GridLength(54, GridUnitType.Pixel);
        }

        private void DownPanelToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            RowDownPanel.Height = new GridLength(0, GridUnitType.Pixel);
        }

        private GridLength w = new GridLength(300, GridUnitType.Pixel);

        private void SettingToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            ColumnSettings.Width = w;
        }

        private void SettingToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            w = ColumnSettings.Width;
            ColumnSettings.Width = new GridLength(0, GridUnitType.Pixel);
        }

        private void LogToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            LogColumn.Width = new GridLength(0, GridUnitType.Auto);
            isWriteToLog = true;
        }

        private void LogToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            LogColumn.Width = new GridLength(0, GridUnitType.Pixel); 
            isWriteToLog = false;
            Log.ClearLog();
        }

        private void GPSL1_Unchecked(object sender, RoutedEventArgs e)
        {
            GLONASSL1L2.IsToggleButtonChecked = false;
            Galileo_BeidouL1L2.IsToggleButtonChecked = false;
            GPSL1L2.IsToggleButtonChecked = true;
        }

        private void GLONASSL1_Unchecked(object sender, RoutedEventArgs e)
        {
            Galileo_BeidouL1L2.IsToggleButtonChecked = false;
        }

        private void GalileoL1_Checked(object sender, RoutedEventArgs e)
        {
            GLONASSL1L2.IsToggleButtonChecked = true;
        }

        #endregion

        private void ModemControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            if (ModemControlConnection.ServerConnectionState == ConnectionStates.Disconnected || ModemControlConnection.ServerConnectionState == ConnectionStates.Unknown)
            {
                if (gSMConnection != null)
                    gSMConnection.ComPort = propertiesPropGrid.Local.Common.ComPort_GSM;
            }
            else
            {
                ModemControlConnection.ServerConnectionState = ConnectionStates.Disconnected;
                gSMConnection.ClosePort();
            }
        }
    }
}