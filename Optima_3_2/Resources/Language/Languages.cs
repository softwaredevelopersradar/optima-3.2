﻿using PropertyGridSettings.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Mapsui;
using ModemControl;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {

        private void PropertiesPropGrid_OnLanguageChanged(object sender, Languages e)
        {
            InitializeLangugeDictionary(e);
            DrawAllStations();

            propertyGridStation.Language = propertiesPropGrid.Local.Common.Language;
            propertyGridControlStation.Language = propertiesPropGrid.Local.Common.Language;

            SetLanguageForLog();
            SetLanguageForTable();
        }

        private void SetLanguageForTable()
        {
            Table.SetTranslation((Language)propertiesPropGrid.Local.Common.Language);
        }

        private void SetLanguageForLog()
        {
            Log.SetLanguge((User_Log.Languages)propertiesPropGrid.Local.Common.Language);
        }

        private void InitializeLangugeDictionary(Languages languages)
        {
            var dict = new ResourceDictionary();
            try
            {
                switch (languages)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/Optima_3_2;component/Resources/Language/StringResource.EN.xaml",
                            UriKind.Relative);
                        stationName = "Transmitter";
                        RasterMap.SetResourceLanguage(MapLanguages.EN);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/Optima_3_2;component/Resources/Language/StringResource.RU.xaml",
                            UriKind.Relative);
                        stationName = "Передатчик";
                        RasterMap.SetResourceLanguage(MapLanguages.RU);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/Optima_3_2;component/Resources/Language/StringResource.AZ.xaml",
                            UriKind.Relative);
                        stationName = "Ötürücü";
                        RasterMap.SetResourceLanguage(MapLanguages.AZ);
                        break;
                    default:
                        dict.Source = new Uri("/Optima_3_2;component/Resources/Language/StringResource.EN.xaml",
                            UriKind.Relative);
                        stationName = "Transmitter";
                        RasterMap.SetResourceLanguage(MapLanguages.EN);
                        break;

                }

                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            {
            }
        }

    }
}
