﻿using System.ComponentModel;

namespace Optima_3_2.Enums
{
    public enum StationStatus
    {
        Normal,
        WithoutRadiation,
        Error
    }

    public enum CommandType
    {
        [Description("ON")] ON,
        [Description("OFF")] OFF,
        [Description("POLL")] POLL
    }
}