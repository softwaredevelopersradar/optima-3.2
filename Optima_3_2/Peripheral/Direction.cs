﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Optima_3_2
{
    public class Direction : INotifyPropertyChanged
    {
        private float _azimuth;

        private float _courseAngle;

        private float _error;

        private float _result;

        public float CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (_courseAngle.Equals(value)) return;
                _courseAngle = value;
                UpdateResult();
            }
        }

        public float Azimuth
        {
            get => _azimuth;
            set
            {
                if (_azimuth.Equals(value)) return;
                _azimuth = value;
                OnPropertyChanged();
                UpdateResult();
            }
        }

        public float Error
        {
            get => _error;
            set
            {
                if (_error.Equals(value)) return;
                _error = value;
                UpdateResult();
            }
        }

        public float Result
        {
            get => _result;
            set
            {
                if (_result.Equals(value)) return;
                _result = value;
                OnPropertyChanged();
            }
        }

        private void UpdateResult()
        {
            var res = CourseAngle + Azimuth + Error;
            while (res >= 360)
                res -= 360;

            while (res < 0)
                res += 360;
            Result = res;
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        #endregion
    }
}