﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using GrozaSModelsDBLib;
using UIMap;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        private void MainWindowViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(mainWindowViewModel.DirectionBRD)))
            {
                BRDAzimuth.Text = mainWindowViewModel.DirectionBRD.Azimuth.ToString();
                BRDResult.Text = mainWindowViewModel.DirectionBRD.Result.ToString();
            }

            if (e.PropertyName.Equals(nameof(mainWindowViewModel.DirectionOEM)))
                textAngleOEM.Text = mainWindowViewModel.DirectionOEM.Result.ToString();
        }

        private void UpdateJammerMap(Coord myOwnCoord)
        {
            var _jammersDB = new ObservableCollection<VMJammer>();


            var t = new TableJammerStation();

            t.Id = 1;
            t.Role = StationRole.Own;
            t.Coordinates = myOwnCoord;

            _jammersDB.Add(new VMJammer(t));


            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
            {
                RasterMap.Jammers.Clear();
                RasterMap.Jammers = _jammersDB;
            });

            UpdateMapDirectionOEM(10);
        }
    }
}