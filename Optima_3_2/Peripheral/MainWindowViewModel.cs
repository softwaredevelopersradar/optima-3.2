﻿using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using WPFControlConnection;

namespace Optima_3_2
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private Direction _directionBRD;

        private Direction _directionOEM;

        private ConnectionStates _stateConnectionBRD = ConnectionStates.Unknown;

        private ConnectionStates _stateConnectionOEM = ConnectionStates.Unknown;

        public MainWindowViewModel()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");


            DirectionOEM = new Direction();
            DirectionBRD = new Direction();
        }

        public ConnectionStates StateConnectionOEM
        {
            get => _stateConnectionOEM;
            set
            {
                if (_stateConnectionOEM != value)
                {
                    _stateConnectionOEM = value;
                    OnPropertyChanged();
                }
            }
        }

        public ConnectionStates StateConnectionBRD
        {
            get => _stateConnectionBRD;
            set
            {
                if (_stateConnectionBRD != value)
                {
                    _stateConnectionBRD = value;
                    OnPropertyChanged();
                }
            }
        }

        public Direction DirectionOEM
        {
            get => _directionOEM;
            set
            {
                if (_directionOEM != value)
                {
                    _directionOEM = value;

                    OnPropertyChanged();
                }
            }
        }

        public Direction DirectionBRD
        {
            get => _directionBRD;
            set
            {
                if (_directionBRD != value)
                {
                    _directionBRD = value;
                    OnPropertyChanged();
                }
            }
        }

        public Direction SetAngleBRD(float AngleNeed, Direction DirectionInput)
        {
            var dif = AngleNeed > DirectionInput.Result
                ? AngleNeed - DirectionInput.Result
                : 360 - (DirectionInput.Result - AngleNeed);

            var val = DirectionInput.Azimuth + dif;
            while (val > 360)
                val -= 360;

            var DirectionOut = new Direction
                { Error = DirectionInput.Error, CourseAngle = DirectionInput.CourseAngle, Azimuth = val };
            return DirectionOut;
        }

        public Direction SetAngleOEM(float AngleNeed, Direction DirectionInput)
        {
            var DirectionOut = new Direction
            {
                Error = DirectionInput.Error * -1, CourseAngle = DirectionInput.CourseAngle * -1, Azimuth = AngleNeed
            };
            return DirectionOut;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        #endregion
    }
}