﻿using System.IO.Ports;
using System.Windows;
using CNTLib;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        public ComARD comARD;

        private void ConnectARD()
        {
            if (comARD != null)
                DisconnectARD();

            comARD = new ComARD();


            comARD.OnConnectPort += ComARD_OnConnectPort;
            comARD.OnDisconnectPort += ComARD_OnDisconnectPort;
            comARD.OnGetAngleReceived += ComARD_OnGetAngleReceived;
            comARD.OnSetAngleReceived += ComARD_OnSetAngleReceived;
            comARD.OnReadByte += ComARD_OnShowReadByteARD;
            comARD.OnWriteByte += ComARD_OnShowWriteByteARD;
            comARD.OnERRReceived += ComARD_OnERRReceivedARD;
            comARD.OnACKReceived += ComARD_OnACKReceivedARD;
            comARD.OnSTPReceived += ComARD_OnSTPReceived;

            comARD.OpenPort(propertiesPropGrid.Local.BRD.ComPort, 19200, Parity.None, 8, StopBits.One);
        }


        private void DisconnectARD()
        {
            if (comARD != null)
            {
                comARD.ClosePort();

                comARD.OnConnectPort -= ComARD_OnConnectPort;
                comARD.OnDisconnectPort -= ComARD_OnDisconnectPort;
                comARD.OnGetAngleReceived -= ComARD_OnGetAngleReceived;
                comARD.OnSetAngleReceived -= ComARD_OnSetAngleReceived;
                comARD.OnSendCmd -= ComARD_OnSendCmd;
                comARD.OnSTPReceived -= ComARD_OnSTPReceived;

                comARD = null;
            }
        }
    }
}