﻿using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using GrozaSModelsDBLib;
using WPFControlConnection;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        private void BRDControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comARD != null)
                    DisconnectARD();
                else
                    ConnectARD();
            }
            catch
            {
            }
        }


        private void ComARD_OnSTPReceived()
        {
        }

        private void ComARD_OnSendCmd(object obj)
        {
        }

        private void ComARD_OnSetAngleReceived(double dAngle, double dElevation)
        {
        }

        private void ComARD_OnGetAngleReceived(double dAngle, double dElevation)
        {
            if (propertiesPropGrid.Local.BRD.Availability)
                mainWindowViewModel.DirectionBRD.Azimuth = (float)dAngle;
        }

        private void ComARD_OnDisconnectPort()
        {
            BRDControlConnection.ButServerColor = Brushes.Red;
            mainWindowViewModel.StateConnectionBRD = ConnectionStates.Disconnected;
            SetBRDToggleButton.IsEnabled = false;
            GetBRDToggleButton.IsEnabled = false;
            StopBRDToggleButton.IsEnabled = false;
        }

        private void ComARD_OnConnectPort()
        {
            BRDControlConnection.ButServerColor = Brushes.Green;
            mainWindowViewModel.StateConnectionBRD = ConnectionStates.Connected;
            SetBRDToggleButton.IsEnabled = true;
            GetBRDToggleButton.IsEnabled = true;
            StopBRDToggleButton.IsEnabled = true;
        }

        private void ComARD_OnACKReceivedARD()
        {
        }

        private void ComARD_OnERRReceivedARD()
        {
        }

        private void ComARD_OnShowWriteByteARD(byte[] bByte)
        {
        }

        private void ComARD_OnShowReadByteARD(byte[] bByte)
        {
        }

        private void UpdateMapDirectionARD()
        {
            try
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
                {
                    if (RasterMap.Jammers != null && RasterMap.Jammers.Count > 0 &&
                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJamming
                            .Direction = mainWindowViewModel.DirectionBRD.Result;
                    /* RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJamming.Visible = (Math.Abs(mainWindowViewModel.DirectionTX1.Result - mainWindowViewModel.DirectionBRD_TX1.Result) < 3) ? false : true;

                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJammingSecond.Direction = mainWindowViewModel.DirectionBRD_TX2.Result;
                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJammingSecond.Visible = (Math.Abs(mainWindowViewModel.DirectionTX2.Result - mainWindowViewModel.DirectionBRD_TX2.Result) < 3) ? false : true;

                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateLink.Direction = mainWindowViewModel.DirectionBRD_CX1.Result;
                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateLink.Visible = (Math.Abs(mainWindowViewModel.DirectionCX1.Result - mainWindowViewModel.DirectionBRD_CX1.Result) < 3) ? false : true;

                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotatePC.Direction = mainWindowViewModel.DirectionBRD_CX2.Result;
                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotatePC.Visible = (Math.Abs(mainWindowViewModel.DirectionCX2.Result - mainWindowViewModel.DirectionBRD_CX2.Result) < 3) ? false : true;
                       */
                });
            }
            catch
            {
            }
        }

        private void SetBRDToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (comARD != null)
            {
                UpdateMapDirectionARD();

                comARD.SendSetAngle(mainWindowViewModel
                        .SetAngleBRD(propertiesPropGrid.Local.BRD.CourseAngle, mainWindowViewModel.DirectionBRD).Result
                    , 0, true);
            }
        }

        private void StopBRDToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (comARD != null)
            {
                if (propertiesPropGrid.Local.BRD.Availability)
                {
                    mainWindowViewModel.DirectionBRD =
                        mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionBRD.Result,
                            mainWindowViewModel.DirectionBRD);
                    comARD.SendStop();
                }

                Thread.Sleep(1000);
            }
        }

        private void GetBRDToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (comARD != null)
            {
                if (propertiesPropGrid.Local.BRD.Availability)
                    comARD.SendGetAngle();

                Thread.Sleep(1000);
            }
        }

        private void SetAngleBRD(float e)
        {
            try
            {
                comARD.SendSetAngle(mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionBRD).Result, 0,
                    false);

                UpdateMapDirectionARD();
            }
            catch
            {
            }
        }
    }
}