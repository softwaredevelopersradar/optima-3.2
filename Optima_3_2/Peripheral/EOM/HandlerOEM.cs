﻿using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using GrozaSModelsDBLib;
using OEM;
using WPFControlConnection;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        private void RasterMap_OnDirection(object sender, float e)
        {
            if (udpOEM != null && udpOEM.IsConnected)
                SetAngleOEM(e);
        }

        private void OEMControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (udpOEM != null && udpOEM.IsConnected)
                    DisconnectOEM();
                else
                    ConnectOEM();
            }
            catch
            {
            }
        }

        private void ConnectPortOEM(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionOEM = ConnectionStates.Connected;
            OEMControlConnection.ServerConnectionState = mainWindowViewModel.StateConnectionOEM;
        }

        private void DisconnectPortOEM(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionOEM = ConnectionStates.Disconnected;
            OEMControlConnection.ServerConnectionState = mainWindowViewModel.StateConnectionOEM;
        }

        private void ConnectEchoOEM(object sender, EventArgs e)
        {
        }

        private void DisconnectEchoOEM(object sender, EventArgs e)
        {
        }

        private void TargetEchoOEM(object sender, EventArgs e)
        {
        }

        private void AnglesEchoOEM(object sender, AnglesEventArgs e)
        {
            try
            {
                UpdateMapDirectionOEM(e.Azimuth);
            }
            catch
            {
            }
        }

        private void VisionEchoOEM(object sender, AnglesEventArgs e)
        {
            mainWindowViewModel.DirectionOEM.Azimuth = e.Azimuth;
        }

        private void SetAngleOEM(float e)
        {
            try
            {
                udpOEM.SendTargetDesignation(
                    mainWindowViewModel.SetAngleOEM(e, mainWindowViewModel.DirectionOEM).Result, 0);
            }
            catch
            {
            }
        }

        private void HandlerDirectionEvent(object sender, float e)
        {
            if (propertiesPropGrid.Local.BRD.Availability)
                SetAngleBRD(e);
            if (propertiesPropGrid.Local.OEM.Availability)
                SetAngleOEM(e);

            textAngleOEM.Text = e.ToString();
        }

        private void UpdateMapDirectionOEM(float sector)
        {
            try
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate
                {
                    if (RasterMap.Jammers != null && RasterMap.Jammers.Count > 0 &&
                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                    {
                        RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneOptic.Direction =
                            mainWindowViewModel.DirectionOEM.Result;

                        if (sector > 60)
                            RasterMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneOptic.Sector =
                                sector;
                    }
                });
            }
            catch
            {
            }
        }
    }
}