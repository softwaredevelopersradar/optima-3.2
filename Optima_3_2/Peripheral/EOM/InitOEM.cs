﻿using System.Windows;
using OEM;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        public UdpOEM udpOEM;


        private void ConnectOEM()
        {
            if (udpOEM != null)
                DisconnectOEM();

            try
            {
                udpOEM = new UdpOEM();

                udpOEM.OnConnectPort += ConnectPortOEM;
                udpOEM.OnDisconnectPort += DisconnectPortOEM;
                udpOEM.OnConnectEcho += ConnectEchoOEM;
                udpOEM.OnDisconnectEcho += DisconnectEchoOEM;
                udpOEM.OnTargetEcho += TargetEchoOEM;
                udpOEM.OnAnglesEcho += AnglesEchoOEM;
                udpOEM.OnVisionEcho += VisionEchoOEM;

                udpOEM.Connect(propertiesPropGrid.Local.OEM.IpAddressLocal, propertiesPropGrid.Local.OEM.PortLocal,
                    propertiesPropGrid.Local.OEM.IpAddressRemoute, propertiesPropGrid.Local.OEM.PortRemoute);

                RasterMap.ZoneOptic = 2000;
                udpOEM.SendConnect();
            }
            catch
            {
            }
        }

        private void DisconnectOEM()
        {
            if (udpOEM != null)
            {
                udpOEM.Disconnect();

                udpOEM.OnConnectPort -= ConnectPortOEM;
                udpOEM.OnDisconnectPort -= DisconnectPortOEM;
                udpOEM.OnConnectEcho -= ConnectEchoOEM;
                udpOEM.OnDisconnectEcho -= DisconnectEchoOEM;
                udpOEM.OnTargetEcho -= TargetEchoOEM;
                udpOEM.OnAnglesEcho -= AnglesEchoOEM;
                udpOEM.OnVisionEcho -= VisionEchoOEM;

                udpOEM = null;
            }
        }
    }
}